package com.tev;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;


/**
 * Created by
 * Titovskiy.E.Va@omega.sbrf.ru
 * Титовский Евгений Валерьевич {СБТ}
 * version: 0.4 (beta)
 * 19.03.2021
 */
public class ProjectReader {

    public static final String LINE_SEPARATOR = "--------------------------------------------------------------------------------------------";
    public static final String OUTPUT_PATH = "G:\\ANDREW\\УЧЕБА\\ВКР 2021\\Записка\\"; // <-- указать путь до директории
    public static final String OUT_FILE_NAME = "исходный код.txt";
    public static final String UTF_8 = "UTF-8";
    public static final String BAD_MARKER = "|||";

    public static void main(String[] args) throws IOException {
        writeReport(getModules(), getExcludedFiles(), getExcludedLines());
    }

    private static void writeReport(Map<String, ModuleInfo> modules, Set<String> excludedFiles, Set<String> excludedLines)
            throws IOException {
        try (PrintWriter writer = new PrintWriter(OUTPUT_PATH + OUT_FILE_NAME, UTF_8)) {
            for (Map.Entry<String, ModuleInfo> entry : modules.entrySet()) {
                scanModule(writer, entry.getKey(), entry.getValue(), excludedFiles, excludedLines);
            }
            printTableOfContents(modules, writer);
        }
    }

    /**
     * Определяется пользователем.
     *
     * @return список модулей, которые необходимо просканировать.
     */
    private static Map<String, ModuleInfo> getModules() {
        Map<String, ModuleInfo> moduleInfoMap = new LinkedHashMap<>();
        moduleInfoMap.put("G:\\ANDREW\\УЧЕБА\\ВКР 2021\\OnlineOrderAggregator\\src\\main\\java\\sal\\online_order_aggregator", new ModuleInfo());
        moduleInfoMap.put("G:\\ANDREW\\УЧЕБА\\ВКР 2021\\OnlineOrderAggregator\\src\\main\\resources\\static", new ModuleInfo());
        moduleInfoMap.put("G:\\ANDREW\\УЧЕБА\\ВКР 2021\\OnlineOrderAggregator\\src\\main\\resources\\chromeExtension", new ModuleInfo());
        return moduleInfoMap;
    }

    /**
     * Определяется пользователем.
     * Указываются строки, или расширения файлов, или какие-либо символы, или имена папок, для исключения чтения
     * определенных файлов. То есть, если будет встречена хоть одна строка из этого списка в АДРЕСЕ ФАЙЛА,
     * то этот файл будет проигнорирован и не будет записан в целовой отчетный документ.
     *
     * @return коллекция строк.
     */
    private static Set<String> getExcludedFiles() {
        Set<String> filters = new HashSet<>();
        filters.add(".git");
        filters.add(".idea");
        filters.add(".pem");
        filters.add(".crt");
        filters.add(".key");
        filters.add(".bin");
        filters.add(".jks");
        filters.add(".cer");
        filters.add("target");
        filters.add("generated");
        filters.add("templates");
        filters.add("images");
        filters.add("node_modules");
        filters.add(".babelrc");
        filters.add("package-lock");
        filters.add("tslint");
        filters.add("projectReader");
        return filters;
    }

    /**
     * Определяется пользователем.
     * Указываются символы или строки, по которым в случае их нахождение в СТРОКЕ кода, строка полностью игнорируется
     * и не пишется в целовой отчетный документ.
     * @return коллекция строк.
     */
    private static Set<String> getExcludedLines() {
        Set<String> filters = new HashSet<>();
//        filters.add("/ "); // javadoc. (пробел не убирать! чтобы корректно обработать xml.)
//        filters.add("* ");  // javadoc
//        filters.add(" *");  // javadoc
//        filters.add("/*");  // javadoc
//        filters.add("*/");  // javadoc
        return filters;
    }

    private static void scanModule(PrintWriter writer, String scanFolder, ModuleInfo value, Set<String> excludedFiles,
                                   Set<String> excludedLines) throws IOException {
        String projectName = Paths.get(scanFolder).toFile().getName();
        projectName = changeProjectName(projectName);
        System.out.println("scanning project: " + projectName);

        printTitle(writer, projectName, "НАЧАЛО ПРОЕКТА: ");
        scanPath(writer, scanFolder, value, excludedFiles, excludedLines, projectName, Paths.get(scanFolder));
        printTitle(writer, projectName, "КОНЕЦ ПРОЕКТА: ");
    }

    private static String changeProjectName(String name) {
        Map<String, String> map = new HashMap<>(Collections.emptyMap());
        map.put("online_order_aggregator","Бэкенд");
        map.put("static","Фронтенд");
        map.put("chromeExtension","Браузерное расширение");

        return map.getOrDefault(name, name);
    }

    private static void scanPath(PrintWriter writer, String scanFolder, ModuleInfo value, Set<String> excludedFiles,
                                 Set<String> excludedLines, String projectName, Path scanPath) throws IOException {
        try (Stream<Path> pathStream = Files.walk(scanPath)) {
            pathStream
                    .filter(Files::isRegularFile)
                    .filter(path -> excludedFiles.stream().noneMatch(excludeFileName -> path.toString().contains(excludeFileName)))
                    .forEach(path -> {
                        value.filesCount++;
                        printFileSeparator(writer, scanFolder, projectName, path);
                        try {
                            Files.lines(path)
                                    .filter(line -> excludedLines.stream().noneMatch(line::contains))
                                    .forEach(s -> {
                                        writer.println(s);
                                        value.linesCount++;
                                    });
                        } catch (Exception e) {
                            writer.println(BAD_MARKER);
                            System.err.println("Ошибка! Файл не удалось корректно считать:" + path);
                        }
                    });
        }
    }

    private static void printFileSeparator(PrintWriter writer, String scanFolder, String projectName, Path path) {
        writer.println("\n" + LINE_SEPARATOR);
        //writer.println("Проект: " + projectName);
        writer.println("Файл: " + path.toString().substring(scanFolder.length()));
        writer.println(LINE_SEPARATOR + "\n");
    }

    private static void printTitle(PrintWriter writer, String projectName, String title) {
        writer.println("\n" + LINE_SEPARATOR);
        writer.println(title + projectName);
        writer.println(LINE_SEPARATOR + "\n");
    }

    private static void printProjectSeparator(PrintWriter writer) {
        writer.println(LINE_SEPARATOR);
    }

    private static void printTableOfContents(Map<String, ModuleInfo> modules, PrintWriter writer) {
        int totalFiles = 0;
        int totalLines = 0;
        writer.println("Итоговый отчет / Оглавление");
        int i = 0;
        for (Map.Entry<String, ModuleInfo> entry : modules.entrySet()) {
            i++;
            ModuleInfo info = entry.getValue();
            String format = String.format("%s.\tфайлов: %s \tстрок: %s \t модуль: %s ",
                    i, info.filesCount, info.linesCount, new File(entry.getKey()).getName());
            writer.println(format);
            totalFiles += info.filesCount;
            totalLines += info.linesCount;
        }
        writer.println(LINE_SEPARATOR);
        writer.println(String.format("\tфайлов: %s \tстрок: %s", totalFiles, totalLines));
    }

    private static class ModuleInfo {
        int filesCount;
        int linesCount;
    }
}
