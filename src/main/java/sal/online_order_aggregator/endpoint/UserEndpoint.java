package sal.online_order_aggregator.endpoint;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import sal.online_order_aggregator.da.UserRepository;
import sal.online_order_aggregator.entity.User;
import sal.online_order_aggregator.util.Logger;

@Service
@RestController
@RequestMapping(value = "/api")
public class UserEndpoint implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/healthcheck", method = RequestMethod.GET)
    public String healthcheck() {
        return "Health is OK";
    }

    private User getUser(String username) {
        User user = userRepository.findByUsername(username);

        if (user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException("User " + username + " not found!");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return getUser(username);
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public ResponseEntity<String> addUser(@RequestBody String user) {
        Gson g = new Gson();
        User newUser = g.fromJson(user, User.class);
        newUser.setPassword(new BCryptPasswordEncoder().encode(newUser.getPassword()));

        try {
            userRepository.save(newUser);

            Logger.log("Создан новый пользователь с логином " + newUser.getUsername());

            HttpHeaders headers = new HttpHeaders();
            headers.add("error", "none");

            return new ResponseEntity<>(headers, HttpStatus.OK);
        } catch (Exception e) {
            HttpHeaders headers = new HttpHeaders();

            if (e.getCause().getCause().getMessage().contains("Нарушение уникального индекса или первичного ключа")) {
                headers.add("error", "username_exists");
                Logger.log("Попытка создания пользователя с уже существующим именем " + newUser.getUsername());
            } else {
                headers.add("error", "unknownError");
                Logger.log("Ошибка при попытке создания пользователя: " + e.getMessage());
            }

            return new ResponseEntity<>(headers, HttpStatus.OK);
        }
    }
}


