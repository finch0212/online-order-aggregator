package sal.online_order_aggregator.da;

import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.stereotype.Component;
import sal.online_order_aggregator.entity.User;

@Component
public class RepositoriesConfig implements RepositoryRestConfigurer {
    /**
     * Сохраняет айдишники в объектах
     */
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(User.class);
    }
}

