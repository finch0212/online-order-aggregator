package sal.online_order_aggregator.endpoint;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

public class UtilsTest {

    @Test
    public void getDeliveryTimeRating() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(5);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getDeliveryTime() throws InterruptedException  {
        TimeUnit.MILLISECONDS.sleep(16);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getMarketplaceRatings_NoData() throws InterruptedException  {
        TimeUnit.MILLISECONDS.sleep(21);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getMarketplaceRatings_OnlyOverall() throws InterruptedException  {
        TimeUnit.MILLISECONDS.sleep(36);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getMarketplaceRatings_OnlyUser() throws InterruptedException  {
        TimeUnit.MILLISECONDS.sleep(31);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getMarketplaceRatings() throws InterruptedException  {
        TimeUnit.MILLISECONDS.sleep(41);
        Assertions.assertEquals(1, 1);
    }
}