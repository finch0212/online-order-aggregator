const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');

let path = require('path');
let webpack = require('webpack');

const BACKEND_PORT = 1001;

module.exports = {
    devtool: 'source-map',
    entry: './app/app.js',
    output: {
        publicPath: '/',
        path: path.join(__dirname, 'generated'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx', '.css', '.png'],
        plugins: [new TsconfigPathsPlugin()]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery', 'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
        }), new CopyWebpackPlugin({
            patterns: [
                {
                    from: 'node_modules/semantic-ui-css/themes/default/assets/fonts/',
                    to: 'vendors/themes/default/assets/fonts'
                },
                {from: 'node_modules/semantic-ui-css/semantic.min.css', to: 'vendors/semantic.min.css'},
            ],
        }),
        new HtmlWebpackPlugin({
            template: "./index.html",
            semanticCSS: `vendors/semantic.min.css`,
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'generated'),
        compress: true,
        historyApiFallback: true,
        disableHostCheck: true,
        port: 9000,
        proxy: {
            '/api': `http://localhost:${BACKEND_PORT}`
        }
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                use: [{
                    loader: 'ts-loader',
                }],
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    'react-hot-loader/webpack',
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                        },
                    },
                ],
                include: [
                    path.resolve(__dirname, 'src'),
                ],
            },
            {
                test: /\.css$/,
                loader: 'style-loader'
            },
            {
                test: /\.css$/,
                loader: 'css-loader',
            },
            {
                test: /\.css$/,
                loader: 'postcss-loader',
                options: {
                    ident: 'postcss',
                    plugins: () => [
                        autoprefixer,
                        require('postcss-import'),
                        require('precss'),
                    ]
                }
            },
            {
                test: /\.eot|ttf|woff|woff2|otf($|\?)/,
                use: [{
                    loader: 'file-loader',
                }],
            },
            {
                test: /\.(png|jpe?g|svg|gif|ico)$/,
                use: ['file-loader?name=images/[name].[ext]']
            }
        ]
    },
};
