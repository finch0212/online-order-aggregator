export interface IOrder {
    id?: string,
    userId?: string,
    link: string,
    name: string,
    orderDate: string,
    receiveDate: string,
    orderId: string,
    itemId: string,
    itemCount: number,
    picture: string,
    price: string,
    currency: string,
    warranty: string,
    marketplaceName: string,
    track: string,
    received: boolean,
    rated?: boolean,
}

export interface IStats {
    orderCount: number,
    moneySpentRub: number,
    moneySpentUsd: number,
    averagePriceRub: number,
    averagePriceUsd: number,
    averageDeliveryTime: number,
    ratedOrdersPercent: number,
}

export interface IMarketplaceRating {
    name: string,
    rating: number,
    qualityRating: number,
    priceRating: number,
    deliveryTimeRating: number,
}

export interface ISortParams {
    sort: string,
    dir: string,
}

export enum EMarketplaces {
    DNS = 'DNS',
    AliExpress = 'AliExpress',
    Other = 'Other',
}

export enum ECurrencies {
    USD = 'USD',
    RUB = 'RUB',
}

export enum EPages {
    DASHBOARD = '/dashboard',
    STATISTICS = '/statistics',
    MARKETPLACE_RATING = '/marketplaceRating',
    SIGNIN = '/signin',
}

export enum ESortableTableColumns {
    NAME = 'name',
    ORDER_DATE = 'orderDate',
    PRICE = 'price',
    STATUS = 'received',
    MARKETPLACE = 'marketplaceName',
}

export enum EUserOrderParamNames {
    marketplace = 'Магазин',
    link = 'Ссылка на страницу товара',
    name = 'Наименование товара',
    orderDate = 'Дата заказа',
    receiveDate = 'Дата получения',
    orderId = 'Номер заказа',
    itemId = 'Идентификатор товара',
    picture = 'Ссылка на картинку',
    price = 'Цена товара',
    currency = 'Валюта',
    itemCount = 'Количество',
    warranty = 'Гарантийный срок',
    track = 'Трек-номер посылки',
    received = 'Статус заказа',
}

export const DATE_FORMAT: string = 'DD.MM.YYYY';
