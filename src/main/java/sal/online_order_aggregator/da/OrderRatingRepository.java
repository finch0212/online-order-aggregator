package sal.online_order_aggregator.da;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import sal.online_order_aggregator.entity.OrderRating;

import java.util.List;
import java.util.Optional;

public interface OrderRatingRepository extends PagingAndSortingRepository<OrderRating, Long> {
    Optional<OrderRating> findById(Long id);

    @Query(value = "select floor(random() * 9999999999999999) as ORDER_ID, 0 as USER_ID, MARKETPLACE_NAME, AVG(Cast(QUALITY_RATING as Float)) as QUALITY_RATING, AVG(Cast(PRICE_RATING as Float)) as PRICE_RATING, AVG(Cast(DELIVERY_TIME_RATING as Float)) as DELIVERY_TIME_RATING from ORDER_RATINGS group by MARKETPLACE_NAME", nativeQuery = true)
    List<OrderRating> getAvgRatings();

    @Query(value = "select floor(random() * 9999999999999999) as ORDER_ID, 0 as USER_ID, MARKETPLACE_NAME, AVG(Cast(QUALITY_RATING as Float)) as QUALITY_RATING, AVG(Cast(PRICE_RATING as Float)) as PRICE_RATING, AVG(Cast(DELIVERY_TIME_RATING as Float)) as DELIVERY_TIME_RATING from ORDER_RATINGS where USER_ID = ?1 group by MARKETPLACE_NAME", nativeQuery = true)
    List<OrderRating> getAvgOrderRatingsByUser(Long userId);
}
