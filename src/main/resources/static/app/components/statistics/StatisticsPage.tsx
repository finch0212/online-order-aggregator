import * as React from 'react';
import {EPages, IStats} from '../interfaces';
import {getUsername, isUserSignedIn} from '../Utils';
import {StatisticsCard} from './StatisticsCard';
const axios = require('axios');

interface IState {
    stats: IStats,
}

interface IProps {
    history: any;
}

export class StatisticsPage extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            stats: null,
        };
    }

    componentDidMount(): void {
        const username = getUsername();

        if (!isUserSignedIn()) {
            window.location.pathname = EPages.SIGNIN;
            setTimeout(() => document.location.reload(), 100);
        } else {
            axios.get('api/getStats', {
                params: {username},
            })
                .then((response) => {
                    this.setState({stats: response.data});
                })
                .catch((error) => {
                });
        }
    }

    render() {
        const {stats} = this.state;

        return (
            <>
                {stats &&
                <div className={'stats-container'}>
                    <StatisticsCard icon="box" color="brown" name="Заказов" value={stats.orderCount} decimals={0}/>
                    <StatisticsCard icon="box" color="brown" name="Дней ожидания в среднем"
                                    value={stats.averageDeliveryTime} decimals={1}/>
                    <StatisticsCard icon="rub" color="green" name="Потрачено средств в рублях"
                                    value={stats.moneySpentRub} decimals={1}/>
                    <StatisticsCard icon="dollar" color="green" name="Потрачено средств в долларах"
                                    value={stats.moneySpentUsd} decimals={1}/>
                    <StatisticsCard icon="rub" color="green" name="Средняя сумма покупки в рублях"
                                    value={stats.averagePriceRub} decimals={1}/>
                    <StatisticsCard icon="dollar" color="green" name="Средняя сумма покупки в долларах"
                                    value={stats.averagePriceUsd} decimals={1}/>
                    <StatisticsCard icon="star" color="yellow" name="Оценено заказов" value={stats.ratedOrdersPercent}
                                    decimals={1} suffix=" %"/>
                </div>
                }
            </>
        );
    }

}
