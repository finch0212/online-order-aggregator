import * as React from 'react';
import {Button, Input, Select} from 'semantic-ui-react';
import {EMarketplaces, IOrder} from '../interfaces';
import {AddNewOrderModal} from './AddNewOrderModal';
import {RateOrderModal} from './RateOrderModal';

interface IProps {
    market: EMarketplaces | 'none';
    loadPage: (pageNum: number, marketPlace?: EMarketplaces | 'none', newFilter?: string) => void;
    changeFilter: (e, data) => void;
    changeMarket: (e, data) => void;
    filter: string;
    reloadPage: () => void;
}

export class SearchBar extends React.PureComponent<IProps, {}> {
    orderModalRef;
    rateOrderModalRef;

    openModal = (order: IOrder) => {
        this.orderModalRef.openForEdit(order);
    };

    render() {
        const {market, loadPage, filter, changeFilter, changeMarket, reloadPage} = this.props;

        return (
            <div>
                <Input
                    placeholder="Поиск"
                    style={{width: 400}}
                    onChange={changeFilter}
                    action
                    type="text"
                >
                    <input value={filter} onKeyDown={(e) => {
                        if (e.which == 13 || e.keyCode == 13) {
                            loadPage(1);
                        }
                    }}/>
                    <Button onClick={() => loadPage(1, market, '')} icon="x" basic/>
                    <Button icon="search" basic onClick={() => loadPage(1)}/>
                </Input>

                <Select
                    style={{margin: '0 20px'}}
                    options={
                        [{
                            key: -1,
                            value: 'none',
                            text: 'Все магазины'
                        }].concat(Object.keys(EMarketplaces).map((marketName, index) => {
                            return {key: index, value: marketName, text: EMarketplaces[marketName]};
                        }))
                    }
                    value={market}
                    onChange={changeMarket}
                />

                <AddNewOrderModal reloadPage={reloadPage} onAddNewReceivedOrder={this.onAddNewReceivedOrder} ref={(x) => this.orderModalRef = x}/>
                <RateOrderModal ref={(x) => this.rateOrderModalRef = x} reloadPage={this.props.reloadPage}/>
            </div>
        );
    }

    onAddNewReceivedOrder = (order: IOrder) => {
        this.rateOrderModalRef.open(order);
    };
}
