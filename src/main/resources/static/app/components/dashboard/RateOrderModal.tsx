import * as React from 'react';
import {Button, Input, Modal, Rating} from 'semantic-ui-react';
import {DATE_FORMAT, EUserOrderParamNames, IOrder} from '../interfaces';
import {getUsername} from '../Utils';
import moment = require('moment');
const axios = require('axios');

interface IProps {
    reloadPage: () => void;
}

interface IState {
    order: IOrder;
    fillReceiveDate: boolean;
    modalOpen: boolean;
    priceRating: number;
    qualityRating: number;
    receiveDate: string;
}

export class RateOrderModal extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            order: null,
            fillReceiveDate: false,
            modalOpen: false,
            priceRating: 0,
            qualityRating: 0,
            receiveDate: null,
        };
    }

    open = (order: IOrder, fillReceiveDate?: boolean) => {
        this.setState({modalOpen: true, fillReceiveDate, order, receiveDate: null});
    };

    render() {
        let {modalOpen, order} = this.state;

        return (
            <Modal
                open={modalOpen}
                onClose={() => this.setState({modalOpen: false})}
                style={{width: 460}}
            >
                <Modal.Header>Оценить заказ</Modal.Header>
                <Modal.Content className={'spaced'}>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <div>
                            <div style={{marginBottom: 5}}>Оцените качество товара</div>
                            <Rating maxRating={5} defaultRating={0} icon="star" size="massive"
                                    onRate={this.onQualityRatingChange}/>
                        </div>

                        <div>
                            <div style={{marginBottom: 5}}>Оцените стоимость товара</div>
                            <Rating maxRating={5} defaultRating={0} icon="star" size="massive"
                                    onRate={this.onPriceRatingChange}/>
                        </div>
                    </div>
                    {this.state.fillReceiveDate &&
                    <>
                        <Input style={{width: '100%'}}
                               disabled
                               type={'date'}
                               value={moment(order.orderDate, DATE_FORMAT).format('YYYY-MM-DD')}
                               placeholder={EUserOrderParamNames.orderDate}
                        />

                        <Input style={{width: '100%'}}
                               type={'date'}
                               onChange={(e, ch) => {
                                   this.setState({receiveDate: ch.value});
                               }}
                               value={this.state.receiveDate}
                               placeholder={EUserOrderParamNames.receiveDate}
                        />
                    </>
                    }

                    <Button basic color={'blue'} content={'Сохранить'} onClick={this.onSave}/>
                </Modal.Content>
            </Modal>
        );
    }

    onPriceRatingChange = (e, {rating}) => {
        this.setState({priceRating: rating});
    };

    onQualityRatingChange = (e, {rating}) => {
        this.setState({qualityRating: rating});
    };

    onSave = () => {
        if (!this.isRatingsValid()) {
            alert('Укажите требуемые данные');
        }

        this.sendRatings();
    };

    isRatingsValid = () => {
        return this.state.priceRating > 0 && this.state.qualityRating > 0
            && (this.state.fillReceiveDate ? this.state.receiveDate !== null : true);
    };

    sendRatings = () => {
        const username = getUsername();
        const {priceRating, qualityRating, order: {id}, receiveDate} = this.state;

        axios.post(`api/addRatings`, {id, priceRating, qualityRating, receiveDate, username}, {
            timeout: 5000,
        })
            .then((response) => {
                this.setState({modalOpen: false});
                this.props.reloadPage();
            })
            .catch((error) => {
            });
    };
}
