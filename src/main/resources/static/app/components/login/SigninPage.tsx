import * as React from 'react';
import {Button, Input, Segment} from 'semantic-ui-react';
import {getFullCSRFParam, saveCookie} from "../Utils";

interface IState {
    password: String;
    username: String;
    logout: boolean;
    error: boolean;
}

interface IProps {
    history: any;
}

const w100: React.CSSProperties = {width: '100%'};

export class SigninPage extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            password: '',
            username: '',
            logout: false,
            error: false
        };
    }

    render() {
        return (
            <div className={'signin-wrapper'}>
                <div className={'signin'}>
                    <Segment className={'spaced'} style={{width: 300, padding: '40px 30px'}}>
                        <div style={{
                            width: '100%',
                            textAlign: 'center',
                            fontWeight: 'bold',
                            fontSize: 16,
                            paddingBottom: 18
                        }}>
                            ONLINE ORDER AGGREGATOR
                        </div>

                        <Input placeholder={'Логин'} style={w100} onChange={this.onChangeLogin}/>

                        <Input placeholder={'Пароль'} style={w100} onChange={this.onChangePass}/>

                        <Button color={'blue'} content={'Вход'} style={w100} onClick={this.login}/>

                        <Button color={'green'} content={'Регистрация'} style={{...w100, ...{marginTop: 20}}}
                                onClick={() => this.props.history.push('/register')}/>
                    </Segment>
                </div>
            </div>
        );
    }

    onChangeLogin = (e, input) => {
        this.setState({username: input.value});
    };

    onChangePass = (e, input) => {
        this.setState({password: input.value});
    };

    login = () => {
        const {username, password} = this.state;

        fetch(`api/perform_login?` +
            `username=${username}` + `&` +
            `password=${password}` + `&` +
            getFullCSRFParam(), {
            method: 'POST'
        }).then((response) => {
            if (!this.refreshPageParams(new URL(response.url))) {
                if (!this.state.error) {
                    this.saveUserAndRedirect();
                }
            }
        });
    };

    saveUserAndRedirect() {
        const {username} = this.state;

        saveCookie('username', username);

        this.props.history.push('/dashboard');
    }

    refreshPageParams(url) {
        this.setState({
            logout: false,
            error: false
        });
        if (url.searchParams.get("error") !== null) {
            alert('Ошибка входа');
            this.setState({error: true});
        }
        if (url.searchParams.get("logout") !== null) {
            alert('Выход выполнен успешно');
            this.setState({logout: true});
        }
        if (!this.state.error && !this.state.logout) {
            return false;
        }
    }
}
