let parse = document.getElementById('parse');
let siteSupported = document.getElementById('siteSupported');
let siteNotSupported = document.getElementById('siteNotSupported');

init();

function init() {
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        const url = tabs[0].url;

        if (isSiteSupported(url)) {
            extentionOn()
        } else {
            extentionOff();
        }
    });
}

function isSiteSupported(url) {
    return url.indexOf(SupportedStoreSites.DNS) !== -1
        || url.indexOf(SupportedStoreSites.TECHNOPOINT) !== -1
        || url.indexOf(SupportedStoreSites.ALIEXPRESS) !== -1;
}

function extentionOn() {
    siteSupported.style.display = 'block';
    siteNotSupported.style.display = 'none';
    parse.style.display = 'block';
}

function extentionOff() {
    siteSupported.style.display = 'none';
    siteNotSupported.style.display = 'block';
    parse.style.display = 'none';
}

parse.onclick = function () {
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        tab = tabs[0];

        chrome.tabs.sendMessage(tab.id, {url: tab.url});
    });
};

//TODO: принимать сообщение от бэка и красить кнопку в зеленый
