import * as React from 'react';
import {Button, Input, Modal} from 'semantic-ui-react';
const axios = require('axios');

interface IProps {
    reloadPage: () => void;
}

interface IState {
    warranty: string;
    modalOpen: boolean;
    editedOrderId: string;
}

export class AddWarrantyModal extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            warranty: '',
            modalOpen: false,
            editedOrderId: null,
        };
    }

    open = (editedOrderId: string, warranty: string) => {
        this.setState({modalOpen: true, editedOrderId, warranty});
    };

    render() {
        const {} = this.props;
        const {modalOpen, warranty} = this.state;

        return (
            <Modal
                open={modalOpen}
                size="tiny"
                onClose={() => this.setState({modalOpen: false})}
            >
                <Modal.Header>Изменить гарантийный срок</Modal.Header>

                <Modal.Content className={'spaced'}>
                    <Input style={{width: '100%'}}
                           onChange={(e, d) => {
                               this.setState({warranty: d.value});
                           }}
                           value={warranty}
                    />
                    <Button color={'blue'} content={'Изменить'} basic onClick={this.handleWarrantySend}/>
                </Modal.Content>
            </Modal>
        );
    }

    handleWarrantySend = () => {
        const {warranty, editedOrderId: id} = this.state;

        axios.post(`api/addWarranty`, {warranty, id}, {
            timeout: 5000,
        })
            .then((response) => {
                this.setState({modalOpen: false});
                this.props.reloadPage();
            })
            .catch((error) => {
            });
    };
}
