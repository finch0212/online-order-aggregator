package sal.online_order_aggregator.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ORDER_RATINGS")
public class OrderRating {
    @Id
    Long orderId;
    String marketplaceName;
    Long userId;
    Float qualityRating;
    Float priceRating;
    Float deliveryTimeRating;

    public OrderRating() {

    }

    public OrderRating(long orderId, String marketplaceName, Long userId, Float qualityRating, Float priceRating, Float deliveryTimeRating) {
        this.orderId = orderId;
        this.marketplaceName = marketplaceName;
        this.userId = userId;
        this.qualityRating = qualityRating;
        this.priceRating = priceRating;
        this.deliveryTimeRating = deliveryTimeRating;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getMarketplaceName() {
        return marketplaceName;
    }

    public void setMarketplaceName(String marketplaceName) {
        this.marketplaceName = marketplaceName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Float getQualityRating() {
        return qualityRating;
    }

    public void setQualityRating(Float qualityRating) {
        this.qualityRating = qualityRating;
    }

    public Float getPriceRating() {
        return priceRating;
    }

    public void setPriceRating(Float priceRating) {
        this.priceRating = priceRating;
    }

    public Float getDeliveryTimeRating() {
        return deliveryTimeRating;
    }

    public void setDeliveryTimeRating(Float deliveryTimeRating) {
        this.deliveryTimeRating = deliveryTimeRating;
    }
}
