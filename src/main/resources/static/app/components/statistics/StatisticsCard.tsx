import * as React from 'react';
import CountUp from 'react-countup';
import {Header, Icon, SemanticCOLORS, SemanticICONS} from 'semantic-ui-react';

type Props = {
    icon: SemanticICONS;
    color: SemanticCOLORS;
    name: string;
    value: number;
    decimals: number;
    suffix?: string
};

export function StatisticsCard(props: Props) {
    let {value, icon, color, decimals, suffix, name} = props;

    if (value === undefined || isNaN(value)) {
        value = 0;
    }

    return (
        <div className={'stat-widget'}>
            <Header size={'large'}><Icon name={icon} color={color}/> {name}</Header>
            <div className={'stat-number'}>
                <CountUp end={value} duration={1.2} decimals={decimals} suffix={suffix}/>
            </div>
        </div>
    );
}
