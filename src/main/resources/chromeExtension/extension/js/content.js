chrome.runtime.onMessage.addListener(async (message, sender, sendResponse) => {
    let orderList = [];

    if (message.url.indexOf(SupportedStoreSites.DNS) !== -1 ||
        message.url.indexOf(SupportedStoreSites.TECHNOPOINT) !== -1) {
        orderList = await getDnsGoods();
    } else if (message.url.indexOf(SupportedStoreSites.ALIEXPRESS) !== -1) {
        orderList = (await getAliGoods()).reverse();
    }

    if (orderList !== []) {
        chrome.runtime.sendMessage({
            type: "sendOrders",
            message: JSON.stringify(orderList)
        });
    }
});
