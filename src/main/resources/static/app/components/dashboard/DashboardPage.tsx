import * as React from 'react';
import {Pagination, Loader, Message, PaginationProps} from 'semantic-ui-react';
import {EMarketplaces, EPages, IOrder, ISortParams} from '../interfaces';
import {getUsername, isUserSignedIn} from '../Utils';
import {OrderTable} from './OrderTable';
import {SearchBar} from './SearchBar';
const axios = require('axios');

interface IState {
    orders: IOrder[];
    page: number;
    pageSize: number;
    totalPages: number;
    error: string;
    filter: string;
    market: EMarketplaces | 'none';
    sortParam: string;
}

interface IProps {
    history: any;
}

export class DashboardPage extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            orders: null,
            error: null,
            page: 1,
            pageSize: 15,
            totalPages: 1,
            filter: '',
            market: 'none',
            sortParam: null,
        };
    }

    componentDidMount(): void {
        if (!isUserSignedIn()) {
            window.location.pathname = EPages.SIGNIN;
            setTimeout(() => document.location.reload(), 100);
        } else {
            this.loadPage(1);
        }
    }

    searchRef;

    loadPage = (pageNum: number, marketPlace?: EMarketplaces | 'none', newFilter?: string, sortParams?: ISortParams) => {
        const username = getUsername();
        let {pageSize: size, filter, market, sortParam: stateSortParam} = this.state;

        filter = newFilter || newFilter === '' ? newFilter : filter;
        market = marketPlace ? marketPlace : market;
        market = market === 'none' ? null : EMarketplaces[market];

        const sort = sortParams?.sort;
        const dir = sortParams?.dir;
        let sortParam = sortParams ? `sort=${sort},${dir}` : '';
        if (sortParams === undefined) {
            sortParam = stateSortParam;
        }

        axios.get(`api/getOrders?${sortParam ? sortParam : ''}`, {
            params: {username, page: pageNum - 1, size, filter, market},
            timeout: 5000,
        })
            .then((response) => {
                this.setState({
                    orders: response.data.content,
                    error: null,
                    totalPages: response.data.totalPages,
                    page: pageNum,
                    filter,
                    sortParam,
                });
            })
            .catch((error) => {
                this.setState({error: error.message});
            });
    };

    reloadPage = () => {
        const {page, orders, totalPages} = this.state;

        if (orders.length === 1 && totalPages !== 1) {
            this.loadPage(page - 1);
        } else {
            this.loadPage(page);
        }
    };

    handleChangeFilter = (e, data) => {
        this.setState({filter: data.value});
    };

    handleChangeMarket = (e, data) => {
        this.setState({market: data.value});
        this.loadPage(1, data.value as EMarketplaces);
    };

    render() {
        const {orders, error, totalPages, market, page, filter} = this.state;

        return (
            <>
                <SearchBar
                    market={market}
                    filter={filter}
                    loadPage={this.loadPage}
                    changeFilter={this.handleChangeFilter}
                    changeMarket={this.handleChangeMarket}
                    reloadPage={this.reloadPage}
                    ref={(x) => this.searchRef = x}
                />

                <div style={{overflow: 'auto', height: 'calc(100vh  - 120px)', marginTop: 20, marginBottom: 20}}>
                    {orders !== null ?
                        <OrderTable orders={orders} reloadPage={this.reloadPage} loadPage={this.loadPage}
                                    editOrder={this.handleEditOrder}/>
                        :
                        (error ?
                                <Message negative>{error}</Message>
                                :
                                <Loader size={'massive'} active>Загрузка данных</Loader>
                        )
                    }
                </div>

                <div className={'pagination'}>
                    <Pagination
                        totalPages={totalPages}
                        onPageChange={this.handlePaginationChange}
                        activePage={page}
                    />
                </div>
            </>
        );
    }

    handlePaginationChange = (e, {activePage}: PaginationProps) => {
        this.loadPage(activePage as number);
    };

    handleEditOrder = (order: IOrder) => {
        this.searchRef.openModal(order);
    };
}
