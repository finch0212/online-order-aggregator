import * as React from 'react';
import {Link} from 'react-router-dom';
import {Icon} from 'semantic-ui-react';
import {EPages} from './interfaces';
import {deleteAllCookie, isUserSignedIn} from './Utils';

export class Sidebar extends React.PureComponent<{}, {}> {
    render() {
        return (
            <>
                <div className="sidebar">
                    <Link to="/dashboard" className={this.getClassname(EPages.DASHBOARD)}>
                        <div onClick={() => {
                            this.forceUpdate();
                        }}>
                            <span style={this.getTextColor(EPages.DASHBOARD)}>
                                <Icon name="browser" inverted={this.getIconColorInverted(EPages.DASHBOARD)}/>
                                Dashboard
                            </span>
                        </div>
                    </Link>

                    <Link to="/statistics" className={this.getClassname(EPages.STATISTICS)}>
                        <div onClick={() => {
                            this.forceUpdate();
                        }}>
                            <span style={this.getTextColor(EPages.STATISTICS)}>
                            <Icon name="chart pie" inverted={this.getIconColorInverted(EPages.STATISTICS)}/>
                                Statistics
                            </span>
                        </div>
                    </Link>

                    <Link to="/marketplaceRating" className={this.getClassname(EPages.MARKETPLACE_RATING)}>
                        <div onClick={() => {
                            this.forceUpdate();
                        }}>
                            <span style={this.getTextColor(EPages.MARKETPLACE_RATING)}>
                            <Icon name="balance" inverted={this.getIconColorInverted(EPages.MARKETPLACE_RATING)}/>
                                Marketplaces
                            </span>
                        </div>
                    </Link>

                    {isUserSignedIn() ?
                        <Link to="/logout" className={'sidebar-item logout'}>
                            <div onClick={() => {
                                setTimeout(() => {
                                    deleteAllCookie();
                                    document.location.reload();
                                }, 100);
                            }}>
                                <span><Icon name="log out" flipped={'horizontally'}/>Logout</span>
                            </div>
                        </Link>
                        :
                        <Link to="/signin" className={'sidebar-item logout'}>
                            <div onClick={() => {
                                setTimeout(() => {
                                    document.location.reload();
                                }, 100);
                            }}>
                                <span><Icon name="log out"/>Sign In</span>
                            </div>
                        </Link>
                    }
                </div>

                <div>
                    {this.props.children}
                </div>
            </>
        );
    }

    getClassname = (page: EPages) => {
        if (window.location.pathname === page) {
            return 'sidebar-item active';
        }
        if (page === EPages.DASHBOARD && window.location.pathname === EPages.SIGNIN) {
            return 'sidebar-item active';
        }

        return 'sidebar-item';
    };

    getIconColorInverted = (page: EPages): boolean => {
        return window.location.pathname === page;
    };

    getTextColor = (page: EPages) => {
        let color = null;
        if (window.location.pathname === page
            || (page === EPages.DASHBOARD && window.location.pathname === EPages.SIGNIN)) {
            color = 'white';
        }
        return {color: color};
    };
}


