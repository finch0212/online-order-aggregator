package sal.online_order_aggregator.entity;

public class Stats {
    long orderCount;
    float moneySpentRub;
    float moneySpentUsd;
    float averagePriceRub;
    float averagePriceUsd;
    float averageDeliveryTime;
    float ratedOrdersPercent;

    public long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(long orderCount) {
        this.orderCount = orderCount;
    }

    public float getMoneySpentRub() {
        return moneySpentRub;
    }

    public void setMoneySpentRub(float moneySpentRub) {
        this.moneySpentRub = moneySpentRub;
    }

    public float getMoneySpentUsd() {
        return moneySpentUsd;
    }

    public void setMoneySpentUsd(float moneySpentUsd) {
        this.moneySpentUsd = moneySpentUsd;
    }

    public float getAveragePriceRub() {
        return averagePriceRub;
    }

    public void setAveragePriceRub(float averagePriceRub) {
        this.averagePriceRub = averagePriceRub;
    }

    public float getAveragePriceUsd() {
        return averagePriceUsd;
    }

    public void setAveragePriceUsd(float averagePriceUsd) {
        this.averagePriceUsd = averagePriceUsd;
    }

    public float getAverageDeliveryTime() {
        return averageDeliveryTime;
    }

    public void setAverageDeliveryTime(float averageDeliveryTime) {
        this.averageDeliveryTime = averageDeliveryTime;
    }

    public float getRatedOrdersPercent() {
        return ratedOrdersPercent;
    }

    public void setRatedOrdersPercent(float ratedOrdersPercent) {
        this.ratedOrdersPercent = ratedOrdersPercent;
    }
}
