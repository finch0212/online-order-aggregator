chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.type === "sendOrders") {
        if (request.message === '[]') {
            alert("Заказов не обнаружено!");
        } else {
            sendOrders(request.message);
        }
    }
});

const sendOrders = (orders) => {
    return getCookie('username', function (username) {
        getCookie('XSRF-TOKEN', function (token) {
            fetch(`${addOrdersURL}?username=${username}&_csrf=${token}`, {
                method: 'POST',
                body: orders
            }).then((response) => {
                alert(`Заказы успешно отсканированы! Отправлено заказов: ${JSON.parse(orders).length}`);
            }).catch((error) => {
                alert('Ошибка при отправке заказов: ' + error);
            });
        });
    });
};