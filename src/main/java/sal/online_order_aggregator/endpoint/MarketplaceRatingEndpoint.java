package sal.online_order_aggregator.endpoint;

import com.google.common.collect.Sets;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import sal.online_order_aggregator.da.MarketplaceRepository;
import sal.online_order_aggregator.da.OrderRatingRepository;
import sal.online_order_aggregator.da.OrderRepository;
import sal.online_order_aggregator.da.UserRepository;
import sal.online_order_aggregator.entity.*;
import sal.online_order_aggregator.util.OrderUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static sal.online_order_aggregator.util.OrderUtils.gsonParser;

@Service
@RestController
@RequestMapping(value = "/api")
public class MarketplaceRatingEndpoint {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderRatingRepository orderRatingRepository;

    @Autowired
    private MarketplaceRepository marketplaceRepository;

    @RequestMapping(value = "/getRatings", method = RequestMethod.POST)
    public @ResponseBody
    List<MarketplaceRating> getRatings(@RequestBody String info) {
        JsonObject jsonObject = gsonParser.fromJson(info, JsonObject.class);
        int qualityFactor = jsonObject.get("qualityFactor").getAsInt();
        int priceFactor = jsonObject.get("priceFactor").getAsInt();
        int deliveryTimeFactor = jsonObject.get("deliveryTimeFactor").getAsInt();
        int myRatingsFactor = jsonObject.get("myRatingsFactor").getAsInt();

        JsonElement usernameJE = jsonObject.get("username");
        List<OrderRating> avgRatingsByUser = Collections.emptyList();

        if (usernameJE != null) {
            String username = usernameJE.getAsString();
            User user = userRepository.findByUsername(username);
            avgRatingsByUser = orderRatingRepository.getAvgOrderRatingsByUser(user.getUserId());
        }

        List<OrderRating> avgRatings = orderRatingRepository.getAvgRatings();

        Set<Marketplace> marketplaces = Sets.newHashSet(marketplaceRepository.findAll()).stream().filter(marketplace ->
                !marketplace.getName().equals("Other")
        ).collect(Collectors.toSet());

        return OrderUtils.getMarketplaceRatings(marketplaces, avgRatings, avgRatingsByUser, qualityFactor,
                priceFactor, deliveryTimeFactor, myRatingsFactor);
    }

    @RequestMapping(value = "/addRatings", method = RequestMethod.POST)
    public ResponseEntity<String> addRatings(@RequestBody String info) throws ParseException {
        JsonObject jsonObject = gsonParser.fromJson(info, JsonObject.class);
        String id = jsonObject.get("id").getAsString();
        String username = jsonObject.get("username").getAsString();
        int priceRating = jsonObject.get("priceRating").getAsInt();
        int qualityRating = jsonObject.get("qualityRating").getAsInt();
        JsonElement receiveDateObj = jsonObject.get("receiveDate");

        Order order = orderRepository.findById(Long.valueOf(id)).get();
        User user = userRepository.findByUsername(username);

        if (!receiveDateObj.isJsonNull()) {
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date receiveDate = sdf.parse(receiveDateObj.getAsString());
            order.setReceiveDate(receiveDate);
        }

        int deliveryTime = OrderUtils.getDeliveryTime(order);
        int deliveryTimeRating = OrderUtils.getDeliveryTimeRating(deliveryTime);
        OrderRating orderRating = new OrderRating(order.getId(), order.getMarketplaceName(), user.getUserId(),
                (float) qualityRating, (float) priceRating, (float) deliveryTimeRating);

        orderRatingRepository.save(orderRating);
        order.setRated(true);
        orderRepository.save(order);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}


