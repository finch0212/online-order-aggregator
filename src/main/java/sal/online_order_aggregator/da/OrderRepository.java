package sal.online_order_aggregator.da;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import sal.online_order_aggregator.entity.Order;

import java.util.Optional;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {
    Optional<Order> findById(Long id);

    void deleteById(Long id);

    Page<Order> findAllByUserIdAndNameContainsIgnoreCase(Long userId, String filter, Pageable pageable);

    Page<Order> findAllByUserIdAndNameContainsIgnoreCaseAndMarketplaceName(Long userId, String name, String marketplaceName, Pageable pageable);
}
