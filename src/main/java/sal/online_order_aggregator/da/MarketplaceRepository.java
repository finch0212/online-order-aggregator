package sal.online_order_aggregator.da;

import org.springframework.data.repository.CrudRepository;
import sal.online_order_aggregator.entity.Marketplace;

import java.util.Optional;

public interface MarketplaceRepository extends CrudRepository<Marketplace, Long> {
    Optional<Marketplace> findById(Long id);

    Optional<Marketplace> findByName(String marketplaceName);

    Iterable<Marketplace> findAll();
}
