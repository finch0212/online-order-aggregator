class OrderInfo {
    constructor() {
    }

    setName(name) {
        this.name = name;
    }

    setOrderId(orderId) {
        this.orderId = orderId;
    }

    setItemId(itemId) {
        this.itemId = itemId;
    }

    setItemCount(itemCount) {
        this.itemCount = itemCount;
    }

    setOrderDate(orderDate) {
        this.orderDate = orderDate;
    }

    setPrice(price) {
        this.price = price;
    }

    setCurrency(currency) {
        this.currency = currency;
    }

    setLink(link) {
        this.link = link;
    }

    setWarranty(warranty) {
        this.warranty = warranty;
    }

    setPicture(picture) {
        this.picture = picture;
    }

    setMarketplaceName(marketplaceName) {
        this.marketplaceName = marketplaceName;
    }

    setReceived(received) {
        this.received = received;
    }
}

const SupportedStoreSites = {DNS: 'dns-shop.ru', TECHNOPOINT: 'technopoint.ru', ALIEXPRESS: 'aliexpress.'};
const Currencies = {USD: 'USD', RUB: 'RUB'};
const SupportedStoreNames = {DNS: 'DNS', ALIEXPRESS: 'AliExpress'};
Object.freeze(SupportedStoreSites);

const domainURL = 'http://localhost';
const addOrdersURL = `${domainURL}:1001/api/addOrders`;

function getCookie(name, callback) {
    chrome.cookies.get({"url": domainURL, "name": name}, function(cookie) {
        if(callback) {
            callback(cookie.value);
        }
    });
}
