package sal.online_order_aggregator.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "MARKETPLACES")
public class Marketplace {
    @Id
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
