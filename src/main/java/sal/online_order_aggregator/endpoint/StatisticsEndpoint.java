package sal.online_order_aggregator.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import sal.online_order_aggregator.da.OrderRepository;
import sal.online_order_aggregator.da.UserRepository;
import sal.online_order_aggregator.entity.Order;
import sal.online_order_aggregator.entity.Stats;
import sal.online_order_aggregator.entity.User;
import sal.online_order_aggregator.util.OrderUtils;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RestController
@RequestMapping(value = "/api")
public class StatisticsEndpoint {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @RequestMapping(value = "/getStats", method = RequestMethod.GET)
    public @ResponseBody
    Stats getStats(@RequestParam(value = "username") String username) {
        User user = userRepository.findByUsername(username);

        String filter = "";
        Pageable pageable = PageRequest.of(0, 99999);

        Stats stats = new Stats();
        AtomicReference<Float> moneyRub = new AtomicReference<>((float) 0);
        AtomicReference<Float> moneyUsd = new AtomicReference<>((float) 0);
        AtomicInteger rubOrders = new AtomicInteger();
        AtomicInteger usdOrders = new AtomicInteger();
        AtomicInteger ratedOrders = new AtomicInteger();
        AtomicInteger deliveredOrders = new AtomicInteger();
        AtomicInteger deliveryTimeSum = new AtomicInteger();

        Page<Order> orders = orderRepository.findAllByUserIdAndNameContainsIgnoreCase(user.getUserId(), filter, pageable);
        stats.setOrderCount(orders.getTotalElements());

        orders.forEach(order -> {
            if (order.getCurrency().equals("RUB")) {
                moneyRub.set(moneyRub.get() + order.getPrice());
                rubOrders.getAndIncrement();
            } else {
                moneyUsd.set(moneyUsd.get() + order.getPrice());
                usdOrders.getAndIncrement();
            }

            if (order.getReceiveDate() != null) {
                int deliveryTime = OrderUtils.getDeliveryTime(order);
                deliveryTimeSum.set(deliveryTimeSum.get() + deliveryTime);
                deliveredOrders.getAndIncrement();
            }

            if (order.isRated()) {
                ratedOrders.set(ratedOrders.get() + 1);
            }
        });

        stats.setMoneySpentRub(moneyRub.get());
        stats.setMoneySpentUsd(moneyUsd.get());
        stats.setAveragePriceRub(moneyRub.get() / rubOrders.get());
        stats.setAveragePriceUsd(moneyUsd.get() / usdOrders.get());
        stats.setAverageDeliveryTime(deliveredOrders.get() == 0 ? 0 : (float) deliveryTimeSum.get() / deliveredOrders.get());
        stats.setRatedOrdersPercent((float) ratedOrders.get() / orders.getTotalElements() * 100);

        return stats;
    }
}


