package sal.online_order_aggregator.da;

import org.springframework.data.repository.CrudRepository;
import sal.online_order_aggregator.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
