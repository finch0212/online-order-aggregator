package sal.online_order_aggregator.endpoint;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Класс для бинда первого входа на сайт (первая загрузка сайта не через реакт роутер)
 */
@Controller
public class EntryPoint {
    @RequestMapping(value = "/signin")
    public String signinPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/register")
    public String registerPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/dashboard")
    public String dashboardPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/statistics")
    public String statsPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/marketplaceRating")
    public String marketplaceRatingPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/")
    public String homePage() {
        return "MainHTML";
    }
}
