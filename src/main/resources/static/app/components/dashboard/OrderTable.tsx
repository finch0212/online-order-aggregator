import * as React from 'react';
import {Dropdown, Icon, Image, Label, Table} from 'semantic-ui-react';
import {ECurrencies, EMarketplaces, ESortableTableColumns, IOrder, ISortParams} from '../interfaces';
import {getDeliveryTime, getMarketplaceLabel, getValueOrDash} from '../Utils';
import {AddTrackModal} from './AddTrackModal';
import {AddWarrantyModal} from './AddWarrantyModal';
import {OrderInfoModal} from './OrderInfoModal';
import {RateOrderModal} from './RateOrderModal';
const axios = require('axios');
require("/images/GoodIcon.png");

interface IProps {
    orders: IOrder[];
    reloadPage: () => void;
    loadPage: (pageNum: number, marketPlace?: EMarketplaces | "none", newFilter?: string, sortParams?: ISortParams) => void;
    editOrder: (order: IOrder) => void;
}

interface IState {
    column: string;
    direction: 'ascending' | 'descending';
}

export class OrderTable extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            column: null,
            direction: 'ascending',
        };
    }

    addTrackModalRef;
    addWarrantyModalRef;
    orderInfoModalRef;
    rateOrderModalRef;

    render() {
        const {column, direction} = this.state;

        return (
            <>
                <Table singleLine sortable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell
                                sorted={column === ESortableTableColumns.STATUS ? direction : null}
                                onClick={this.handleSort(ESortableTableColumns.STATUS)}
                                content={'Статус'}
                            />
                            <Table.HeaderCell
                                sorted={column === ESortableTableColumns.MARKETPLACE ? direction : null}
                                onClick={this.handleSort(ESortableTableColumns.MARKETPLACE)}
                                content={'Магазин'}
                            />
                            <Table.HeaderCell content={'Изображение'}/>
                            <Table.HeaderCell
                                sorted={column === ESortableTableColumns.ORDER_DATE ? direction : null}
                                onClick={this.handleSort(ESortableTableColumns.ORDER_DATE)}
                                content={'Дата заказа'}
                            />
                            <Table.HeaderCell
                                content={'Срок доставки'}
                            />
                            <Table.HeaderCell
                                sorted={column === ESortableTableColumns.NAME ? direction : null}
                                onClick={this.handleSort(ESortableTableColumns.NAME)}
                                content={'Наименование'}
                            />
                            <Table.HeaderCell
                                sorted={column === ESortableTableColumns.PRICE ? direction : null}
                                onClick={this.handleSort(ESortableTableColumns.PRICE)}
                                content={'Цена'}
                            />
                            <Table.HeaderCell content={'Гарантия'}/>
                            <Table.HeaderCell content={'Ссылка'}/>
                            <Table.HeaderCell content={'Трек-код'}/>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.props.orders.map(order => this.getOrderRow(order))}
                    </Table.Body>
                </Table>

                <AddTrackModal ref={(x) => this.addTrackModalRef = x} reloadPage={this.props.reloadPage}/>
                <AddWarrantyModal ref={(x) => this.addWarrantyModalRef = x} reloadPage={this.props.reloadPage}/>
                <OrderInfoModal ref={(x) => this.orderInfoModalRef = x}/>
                <RateOrderModal ref={(x) => this.rateOrderModalRef = x} reloadPage={this.props.reloadPage}/>
            </>
        );
    }

    getOrderRow = (order: IOrder) => {
        const name = this.getNameWithCount(order);

        return (
            <Table.Row key={order.id}>
                <Table.Cell width={'1'}>
                    <Label color={order.received ? 'green' : 'orange'} basic style={{width: '100%'}}
                           className={'centered'}>{order.received ? 'Получен' : 'В пути'}</Label>
                </Table.Cell>
                <Table.Cell width={'1'}>{getMarketplaceLabel(order.marketplaceName)}</Table.Cell>
                <Table.Cell width={'1'}>
                    <Image src={order.picture || "/images/GoodIcon.png"} style={{margin: 'auto', height: 32}}/>
                </Table.Cell>
                <Table.Cell width={'1'}>{getValueOrDash(order?.orderDate)}</Table.Cell>
                <Table.Cell
                    width={'1'}
                    style={{color: order.received ? null : 'darkgray'}}>{getValueOrDash(getDeliveryTime(order.orderDate, order.receiveDate, order.received))}
                </Table.Cell>
                <Table.Cell title={name}>{name}</Table.Cell>
                <Table.Cell
                    width={'1'}>{getValueOrDash(order.price)} {order?.currency === ECurrencies.RUB ? '₽' : '$'}
                </Table.Cell>
                <Table.Cell width={'1'}>{getValueOrDash(order.warranty)}</Table.Cell>
                <Table.Cell width={'1'}>{order.link ? <a href={order.link}>Ссылка</a> : '-'}</Table.Cell>
                <Table.Cell width={'2'}>{getValueOrDash(order.track)}</Table.Cell>
                <Table.Cell width={'1'} className={'actions'} textAlign='right'>
                    {order.rated && <Icon name={'star'} style={{marginRight: 10}} color={'yellow'}/>}
                    <Dropdown
                        icon='bars'
                        floating
                        button
                        basic
                        pointing={'top right'}
                        compact
                        className={'icon'}
                    >
                        <Dropdown.Menu>
                            <Dropdown.Item
                                icon='info circle'
                                text='Все сведения'
                                onClick={() => {
                                    this.orderInfoModalRef.open(order);
                                }}
                            />

                            <Dropdown.Item
                                icon='box'
                                text='Указать трек-код'
                                onClick={() => {
                                    this.addTrackModalRef.open(order.id, order.track);
                                }}
                            />

                            <Dropdown.Item
                                icon='file text'
                                text='Указать гарантийный срок'
                                onClick={() => {
                                    this.addWarrantyModalRef.open(order.id, order.warranty);
                                }}
                            />

                            <Dropdown.Item
                                icon='map marker'
                                text='Сменить статус на "Получен"'
                                onClick={() => {
                                    this.handleSetDelivered(order);
                                }}
                                disabled={order.received}
                            />

                            <Dropdown.Item
                                icon='star'
                                text='Оценить'
                                onClick={() => {
                                    this.rateOrderModalRef.open(order, !order.receiveDate);
                                }}
                                disabled={order.rated || !order.received || order.marketplaceName === EMarketplaces.Other}
                            />

                            <Dropdown.Item
                                icon='edit'
                                text='Изменить'
                                onClick={() => {
                                    this.props.editOrder(order);
                                }}
                            />

                            <Dropdown.Item onClick={() => this.handleDeleteOrder(order.id)}>
                                <Icon name={'trash'} color={'red'}/>Удалить
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Table.Cell>
            </Table.Row>
        );
    };

    handleSetDelivered = (order: IOrder) => {
        axios.post(`api/setDelivered`, {id: order.id}, {
            timeout: 5000,
        })
            .then((response) => {
                if (order.marketplaceName !== EMarketplaces.Other) {
                    this.rateOrderModalRef.open(order);
                }
            })
            .catch((error) => {});
    };

    handleDeleteOrder = (id: string) => {
        axios.post(`api/deleteOrder`, {id}, {
            timeout: 5000,
        })
            .then((response) => {
                this.props.reloadPage();
            })
            .catch((error) => {});
    };

    handleSort = (columnName: string) => () => {
        const {column, direction} = this.state;
        let newDirection: 'ascending' | 'descending';

        if (column === columnName) {
            if (direction) {
                newDirection = direction === 'descending' ? 'ascending' : null;
            } else {
                newDirection = 'descending';
            }
        } else {
            newDirection = 'descending';
        }

        this.setState({
            column: columnName,
            direction: newDirection,
        });

        const dir = newDirection === 'ascending' ? 'asc' : 'desc';
        const sortParams = newDirection ? {sort: columnName, dir} : null;

        this.props.loadPage(1, null, null, sortParams);
    };

    getNameWithCount = (order: IOrder) => {
        let name = getValueOrDash(order.name);

        if (order.itemCount && order?.itemCount > 1) {
            name = name.concat(` [${order.itemCount} шт.]`);
        }

        return name;
    };
}
