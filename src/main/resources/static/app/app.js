import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import {SigninPage} from "./components/login/SigninPage";
import {RegisterPage} from "./components/register/RegisterPage";
import {DashboardPage} from "./components/dashboard/DashboardPage";
import {StatisticsPage} from "./components/statistics/StatisticsPage";
import {MarketplaceRating} from "./components/marketplaceRating/MarketplaceRating";
import {Sidebar} from "./components/Sidebar";
import '../css/main.css';
import '../css/semanticHacks.css';

export class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Route exact path='/signin' component={SigninPage}/>
                    <Route exact path='/register' component={RegisterPage}/>
                    <Sidebar>
                        <Route exact path='/dashboard' component={DashboardPage}/>
                        <Route exact path='/statistics' component={StatisticsPage}/>
                        <Route exact path='/marketplaceRating' component={MarketplaceRating}/>
                        <Route exact path='/' component={MarketplaceRating}/>
                    </Sidebar>
                </div>
            </BrowserRouter>
        )
    }
}

const el = document.getElementById('root');
ReactDOM.render(<App/>, el);
