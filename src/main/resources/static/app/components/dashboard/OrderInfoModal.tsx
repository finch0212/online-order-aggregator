import * as React from 'react';
import {Grid, GridColumn, Image, Label, Modal} from 'semantic-ui-react';
import {ECurrencies, IOrder} from '../interfaces';
import {getDeliveryTime, getLabeledText, getMarketplaceLabel, getValueOrDash} from '../Utils';

interface IState {
    order: IOrder;
    modalOpen: boolean;
}

export class OrderInfoModal extends React.PureComponent<{}, IState> {
    constructor(props) {
        super(props);

        this.state = {
            order: null,
            modalOpen: false,
        };
    }

    open = (order: IOrder) => {
        this.setState({modalOpen: true, order});
    };

    render() {
        let {modalOpen, order} = this.state;

        return (
            <Modal
                open={modalOpen}
                onClose={() => this.setState({modalOpen: false})}
            >
                <Modal.Header>Полная информация о заказе</Modal.Header>
                <Modal.Content>
                    <Grid>
                        <GridColumn width={6}>
                            <Image src={order?.picture} style={{margin: '0 auto 20px auto'}}/>
                            {getMarketplaceLabel(order?.marketplaceName)}
                            <Label basic color={'blue'}
                                   className={'price'}>{order?.price} {order?.currency === ECurrencies.RUB ? '₽' : '$'}</Label>
                        </GridColumn>

                        <GridColumn width={10}>
                            {getLabeledText('Наименование', `${order?.name} x${order?.itemCount}`)}
                            <Grid>
                                <GridColumn width={8}>
                                    {getLabeledText('ID заказа', getValueOrDash(order?.orderId))}
                                    {getLabeledText('Дата заказа', getValueOrDash(order?.orderDate))}
                                    {getLabeledText('Статус', order?.received ? 'Получен' : 'В пути')}
                                    {getLabeledText('Трек-номер', getValueOrDash(order?.track))}
                                </GridColumn>

                                <GridColumn width={8}>
                                    {getLabeledText('ID товара', getValueOrDash(order?.itemId))}
                                    {getLabeledText('Ссылка', <a href={order?.link}>Ссылка</a>)}
                                    {getLabeledText('Срок доставки',getValueOrDash(
                                        getDeliveryTime(order?.orderDate, order?.receiveDate, order?.received)))}
                                    {getLabeledText('Срок гарантии', getValueOrDash(order?.warranty))}
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                    </Grid>
                </Modal.Content>
            </Modal>
        );
    }
}
