package sal.online_order_aggregator.endpoint;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import sal.online_order_aggregator.da.MarketplaceRepository;
import sal.online_order_aggregator.da.OrderRepository;
import sal.online_order_aggregator.da.UserRepository;
import sal.online_order_aggregator.entity.Marketplace;
import sal.online_order_aggregator.entity.Order;
import sal.online_order_aggregator.entity.User;
import sal.online_order_aggregator.util.Logger;

import java.lang.reflect.Type;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static sal.online_order_aggregator.util.OrderUtils.gsonParser;

@Service
@RestController
@RequestMapping(value = "/api")
public class OrderEndpoint {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MarketplaceRepository marketplaceRepository;

    @RequestMapping(value = "/addOrders", method = RequestMethod.POST)
    public ResponseEntity<String> addOrder(@RequestParam(value = "username") String username, @RequestBody String orders) {
        Type type = new TypeToken<List<Order>>() {
        }.getType();
        List<Order> newOrders = gsonParser.fromJson(orders, type);

        String marketplaceName = newOrders.get(0).getMarketplaceName();
        Optional<Marketplace> opt_marketplace = marketplaceRepository.findByName(marketplaceName);
        if (!opt_marketplace.isPresent()) {
            Marketplace marketplace = new Marketplace();
            marketplace.setName(marketplaceName);
            marketplaceRepository.save(marketplace);
        }

        boolean isOnlyOneOrder = newOrders.size() == 1;
        User user = userRepository.findByUsername(username);
        AtomicReference<Order> newOrder = new AtomicReference<>(new Order());

        newOrders.forEach(order -> {
            order.setUserId(user.getUserId());

            try {
                order.setHash();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            try {
                if (isOnlyOneOrder) {
                    newOrder.set(orderRepository.save(order));
                } else {
                    orderRepository.save(order);
                }
            } catch (Exception e) {
                if (e.getCause().getCause().getMessage().contains("Нарушение уникального индекса или первичного ключа")) {
                    Logger.log("Пресечено добавление повторяющегося заказа");
                } else {
                    Logger.log("Ошибка при добавлении заказа: " + e.getMessage());
                    throw e;
                }
            }
        });

        if (isOnlyOneOrder) {
            String newOrderJson = gsonParser.toJson(newOrder);

            return new ResponseEntity<>(newOrderJson, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/changeOrder", method = RequestMethod.POST)
    public ResponseEntity<String> changeOrder(@RequestBody String orders) {
        Type type = new TypeToken<List<Order>>() {
        }.getType();
        List<Order> changingOrders = gsonParser.fromJson(orders, type);
        Order order = changingOrders.get(0);
        Order orderFromDB = orderRepository.findById(Long.valueOf(order.getId())).get();

        orderFromDB.setName(order.getName());
        orderFromDB.setItemCount(order.getItemCount());
        orderFromDB.setPrice(order.getPrice());
        orderFromDB.setReceived(order.isReceived());
        orderFromDB.setReceiveDate(order.getReceiveDate());
        orderFromDB.setLink(order.getLink());
        orderFromDB.setPicture(order.getPicture());
        orderFromDB.setTrack(order.getTrack());
        orderFromDB.setWarranty(order.getWarranty());

        orderRepository.save(orderFromDB);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteOrder", method = RequestMethod.POST)
    public ResponseEntity<String> deleteOrder(@RequestBody String info) {
        JsonObject jsonObject = gsonParser.fromJson(info, JsonObject.class);
        String id = jsonObject.get("id").getAsString();

        orderRepository.deleteById(Long.valueOf(id));

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/getOrders", method = RequestMethod.GET)
    public @ResponseBody
    Page<Order> getOrders(@RequestParam(value = "username") String username, Pageable pageable, String filter, String market) {
        User user = userRepository.findByUsername(username);

        if (filter == null) {
            filter = "";
        }

        if (pageable.getSort().isUnsorted()) {
            pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("id").descending());
        }

        if (market == null || market.equals("none")) {
            return orderRepository.findAllByUserIdAndNameContainsIgnoreCase(user.getUserId(), filter, pageable);
        }

        return orderRepository.findAllByUserIdAndNameContainsIgnoreCaseAndMarketplaceName(user.getUserId(), filter, market, pageable);
    }

    @RequestMapping(value = "/addWarranty", method = RequestMethod.POST)
    public ResponseEntity<String> addWarranty(@RequestBody String info) {
        JsonObject jsonObject = gsonParser.fromJson(info, JsonObject.class);
        String id = jsonObject.get("id").getAsString();
        String warranty = jsonObject.get("warranty").getAsString();

        Order order = orderRepository.findById(Long.valueOf(id)).get();
        order.setWarranty(warranty);
        orderRepository.save(order);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/addTrack", method = RequestMethod.POST)
    public ResponseEntity<String> addTrack(@RequestBody String info) {
        JsonObject jsonObject = gsonParser.fromJson(info, JsonObject.class);
        String id = jsonObject.get("id").getAsString();
        String track = jsonObject.get("track").getAsString();

        Order order = orderRepository.findById(Long.valueOf(id)).get();
        order.setTrack(track);
        orderRepository.save(order);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/setDelivered", method = RequestMethod.POST)
    public ResponseEntity<String> setDelivered(@RequestBody String info) {
        JsonObject jsonObject = gsonParser.fromJson(info, JsonObject.class);
        String id = jsonObject.get("id").getAsString();

        Order order = orderRepository.findById(Long.valueOf(id)).get();
        Date today = new Date();
        order.setReceiveDate(today);
        order.setReceived(true);
        orderRepository.save(order);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}


