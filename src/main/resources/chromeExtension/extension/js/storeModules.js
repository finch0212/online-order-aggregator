//DNS
const getDnsGoods = async () => {
    let orderListForSend = [];
    let orderList = document.querySelectorAll('[class="order-item"]');

    await orderList.forEach(rawOrder => {
            let rawStatus = rawOrder.children.item(2).querySelectorAll('[class="content"]')
                .item(0).children.item(0).textContent;
            let status = rawStatus.replace(/\s/g, '').slice(7, rawStatus.length);

            if (status !== 'Отменен') {
                let received = status === 'Завершен';
                let orderIdAndDate = rawOrder.children.item(0).children.item(0).children
                    .item(0).textContent.replace(/\s/g, '');
                let divider = orderIdAndDate.indexOf('от');
                let orderId = orderIdAndDate.slice(5, divider);
                let rawDate = orderIdAndDate.slice(divider + 2, divider + 12);
                let orderDate = `${rawDate.slice(8)}.${rawDate.slice(5, 7)}.${rawDate.slice(0, 4)}`;

                let imageInfoList = rawOrder.children.item(2).querySelectorAll('[class="image"]');

                imageInfoList.forEach(imageInfo => {
                    let link = imageInfo.children.item(0).getAttribute('href');
                    link = 'https://www.dns-shop.ru' + link.slice(link.indexOf('/product'));
                    let pic = imageInfo.children.item(0).children.item(0).getAttribute('src');
                    let name = imageInfo.children.item(0).children.item(0).getAttribute('alt');

                    let caption = imageInfo.nextElementSibling;
                    let rawItemPrice = caption.children.item(1).textContent.replace(/\s/g, '');
                    let itemPrice = rawItemPrice.slice(0, rawItemPrice.indexOf('₽'));
                    let itemCount = caption.children.item(2).firstElementChild.textContent
                        .slice(12, 13);
                    let rawItemId = caption.children.item(2).children.item(1).textContent;
                    let itemId = rawItemId.slice(12, rawItemId.length - 1);

                    let order = new OrderInfo();

                    order.setOrderId(orderId);
                    order.setItemId(itemId);
                    order.setName(name);
                    order.setOrderDate(orderDate);
                    order.setPrice(itemPrice * itemCount);
                    order.setItemCount(itemCount);
                    order.setPicture(pic);
                    order.setLink(link);
                    order.setCurrency(Currencies.RUB);
                    order.setMarketplaceName(SupportedStoreNames.DNS);
                    order.setReceived(received);

                    orderListForSend.push(order);
                });
            }
        }
    );

    return orderListForSend;
};

//ALI
const getAliGoods = async () => {
    let orderListForSend = [];
    let orderList = document.querySelectorAll('.order-item-wraper');

    await orderList.forEach(rawOrder => {
            let idAndDate = rawOrder.children.item(0).querySelectorAll('.info-body');
            let orderId = idAndDate[0].textContent;
            let rawDate = idAndDate[1].textContent;
            let orderDate = `${rawDate.slice(rawDate.length - 7, rawDate.length - 5)}.${getNumberOfMonth(rawDate.slice(6, 9))}.${rawDate.slice(rawDate.length - 4)}`

            let status = rawOrder.children[1].children[2].firstElementChild.innerHTML.replaceAll(' ', '').replaceAll('\n', '');
            let additionalStatus = rawOrder.children[1].children[1].firstElementChild;
            additionalStatus = additionalStatus ? additionalStatus.innerHTML.replaceAll(' ', '').replaceAll('\n', '') : '';

            let received = true;
            //заказ отменен - не вычитываем
            if (status === 'Закрыт') {
                return;
            }
            //заказ не оплачен/не отправлен/возврат средств - не вычитываем
            if (status === 'Завершено' && !(additionalStatus === 'Полученоподтверждение' || additionalStatus === 'Истекловремяподтверждения')) {
                return;
            }
            //заказ отправлен или отправка ожидается
            if (status === 'Заказотправлен' || status === 'Ожидаетсяотправка') {
                received = false;
            }

            let goods = rawOrder.querySelectorAll('.product-sets');
            goods.forEach(good => {
                let order = new OrderInfo();

                let link = good.children.item(0).children.item(0).getAttribute('href');

                let pic = good.children.item(0).children.item(0).children.item(0).getAttribute('src');
                pic = pic.slice(0, pic.indexOf('50x50')) + '250x250.jpg';

                let name = good.children.item(1).children.item(0).children.item(0).getAttribute('title');
                let props = good.children.item(1).children.item(3).children.item(0);
                if (props) {
                    props = props.children.item(0).textContent.trim();
                    name = `${name} ${props}`;
                }

                let itemPrice = good.children.item(1).children.item(2).children.item(0).textContent;
                if (itemPrice[1] === '$') {
                    itemPrice = itemPrice.slice(3).replace(',', '.').replace(' ', '');
                    order.setCurrency(Currencies.USD);
                } else {
                    itemPrice = itemPrice.slice(1).slice(0, -5).replace(',', '.').replace(' ', '');
                    order.setCurrency(Currencies.RUB);
                }
                let itemCount = good.children.item(1).children.item(2).children.item(1).textContent.slice(1);

                order.setOrderId(orderId);
                //order.setItemId(itemId);
                order.setName(name);
                order.setOrderDate(orderDate);
                order.setPrice(itemPrice * itemCount);
                order.setItemCount(itemCount);
                //order.setWarranty(warranty);
                order.setPicture(pic);
                order.setLink(link);
                order.setMarketplaceName(SupportedStoreNames.ALIEXPRESS);
                order.setReceived(received);

                orderListForSend.push(order);
            });
        }
    );

    return orderListForSend;
};

const getNumberOfMonth = (text) => {
    switch (text) {
        case 'Jan':
            return '01';
        case 'Feb':
            return '02';
        case 'Mar':
            return '03';
        case 'Apr':
            return '04';
        case 'May':
            return '05';
        case 'Jun':
            return '06';
        case 'Jul':
            return '07';
        case 'Aug':
            return '08';
        case 'Sep':
            return '09';
        case 'Oct':
            return '10';
        case 'Nov':
            return '11';
        case 'Dec':
            return '12';
    }
};



