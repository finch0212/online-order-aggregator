import * as React from 'react';
import {Icon, SemanticICONS} from 'semantic-ui-react';
import {IMarketplaceRating} from '../interfaces';
require("/images/DNS.png");
require("/images/AliExpress.png");
require("/images/React.png");

interface IProps {
    marketplace: IMarketplaceRating;
    index: number;
}

export class MarketplaceCard extends React.PureComponent<IProps, {}> {
    render() {
        const {
            index,
            marketplace: {deliveryTimeRating, name, priceRating, qualityRating, rating}
        } = this.props;

        return (
            <div className={`marketplace-widget${index >= 3 ? ' dimmed' : ''}`}>
                <div className={`marketplace-widget-count${index <= 3 ? ' _' + index : ''}`}>
                    {index}
                </div>
                <div className={'marketplace-widget-image'}>
                    <img src={`/images/${name ? name : 'React'}.png`} alt={name} width="180"/>
                </div>
                <div className="marketplace-widget-bars">
                    {this.getBar('star', +qualityRating.toFixed(1))}
                    {this.getBar('dollar', +priceRating.toFixed(1))}
                    {this.getBar('clock outline', +deliveryTimeRating.toFixed(1))}
                </div>
                <div className={'marketplace-widget-score'}>{rating.toFixed(2)}</div>
            </div>
        );
    }

    getBar = (icon: SemanticICONS, rating: number) => {
        return (
            <div className={'marketplace-widget-bar-box'}>
                <Icon name={icon}/>
                <div className={'marketplace-widget-bar'}>
                    <div
                        style={{width: `calc(${rating * 20}% + 2px)`}}
                        className={'marketplace-widget-filled-bar'}
                    />
                </div>
                <div className={'marketplace-widget-bar-rating'}>{rating}</div>
            </div>
        );
    };
}
