import * as React from 'react';
import {Button, Icon, SemanticICONS} from 'semantic-ui-react';
import {IMarketplaceRating} from '../interfaces';
import {getUsername, isUserSignedIn} from '../Utils';
import {MarketplaceCard} from './MarketplaceCard';
const axios = require('axios');

interface IState {
    qualityFactor: number,
    priceFactor: number,
    deliveryTimeFactor: number,
    myRatingsFactor: number,
    marketplaces: IMarketplaceRating[],
}

interface IProps {
    history: any;
}

export class MarketplaceRating extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            deliveryTimeFactor: 5,
            priceFactor: 5,
            qualityFactor: 5,
            myRatingsFactor: 5,
            marketplaces: null,
        };
    }

    getMockMarketplace = (): IMarketplaceRating => {
        return {
            deliveryTimeRating: Math.floor(Math.random() * 50) / 10,
            priceRating: Math.floor(Math.random() * 50) / 10,
            qualityRating: Math.floor(Math.random() * 50) / 10,
            rating: Math.random() * 100 + Math.random(),
            name: null,
        };
    };

    componentDidMount(): void {
        this.getRatings();
    }

    getRatings = () => {
        const username = getUsername();
        const {deliveryTimeFactor, priceFactor, qualityFactor, myRatingsFactor} = this.state;

        axios.post('api/getRatings', {username, qualityFactor, priceFactor, deliveryTimeFactor, myRatingsFactor}, {})
            .then((response) => {
                this.setState({marketplaces: response.data.concat([...Array(14)].map(() => this.getMockMarketplace()))});
            })
            .catch((error) => {
            });
    };

    render() {
        const {marketplaces, qualityFactor, priceFactor, deliveryTimeFactor, myRatingsFactor} = this.state;
        const userSignedIn = isUserSignedIn();

        return (
            <>
                <div className={'marketplace-widget-container'}>
                    {marketplaces && marketplaces.map((mp, index) =>
                        <MarketplaceCard marketplace={mp} index={index + 1} key={index}/>
                    )}
                </div>

                <div className={'options'}>
                    <div className="options-tail">^</div>

                    <div className="options-halfs">
                        <div className="options-left">
                            Коэффициенты
                            {this.getSlider(qualityFactor, 'star', this.onChangeQualityFactor)}
                            {this.getSlider(priceFactor, 'dollar', this.onChangePriceFactor)}
                            {this.getSlider(deliveryTimeFactor, 'clock outline', this.onChangeDeliveryTimeFactor)}
                        </div>

                        <div className="options-right">
                            <div style={userSignedIn ? {} : {opacity: 0.3}}>
                                Коэффициент собственных оценок
                                {this.getSlider(myRatingsFactor, 'user', this.onChangeMyRatingFactor, true, !userSignedIn)}
                            </div>
                            <Button color={'blue'} basic content={'Применить'} onClick={this.getRatings}/>
                        </div>
                    </div>
                </div>
            </>
        );
    }

    getSlider = (factor: number, icon: SemanticICONS, onChange: (e) => void, myFactor?: boolean, disabled?: boolean) => {
        const {myRatingsFactor} = this.state;

        return (
            <div
                className={'options-slider-group'}
                style={myFactor ? {marginBottom: 22} : {}}
            >
                <Icon name={icon}/>
                <input
                    type={'range'}
                    min={0}
                    max={10}
                    value={factor}
                    onChange={onChange}
                    disabled={disabled}
                />
                <div className={'options-slider-group-value'}>{factor}</div>

                {myFactor && (myRatingsFactor == 10 || myRatingsFactor == 0) &&
                <div className={'options-tip'}>
                    {`Учитываются только ${myRatingsFactor == 10 ? 'свои' : 'общие'} оценки`}
                </div>
                }
            </div>
        );
    };

    onChangeQualityFactor = (e) => {
        this.setState({qualityFactor: e.target.value});
    };

    onChangePriceFactor = (e) => {
        this.setState({priceFactor: e.target.value});
    };

    onChangeDeliveryTimeFactor = (e) => {
        this.setState({deliveryTimeFactor: e.target.value});
    };

    onChangeMyRatingFactor = (e) => {
        this.setState({myRatingsFactor: e.target.value});
    };
}
