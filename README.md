# Online Order Aggregator
###### Developed by Sorokin Andrey
### Backend: 
**Tech-stack**: Java, SpringBoot, Hibernate, Lombok, H2  
**Path**: src/main/java
### Frontend: 
**Tech-stack**: React, Webpack, Typescript, HTML, PostCSS, Semantic-ui  
**Path**: src/main/resources/static
### Chrome Extension:
**Tech-stack**: Javascript, HTML, CSS  
**Path**: src/main/resources/chromeExtension

## Описание проекта:

**Online Order Aggregator** - система для ведения личной истории заказов
из различных интернет-магазинов, предоставляющая статистику, а также возможность ранжирования используемых интернет-магазинов.

Система представляет из себя связку веб-сайта и браузерного расширения с общей серверной частью.  
Веб-сайт предоставляет доступ к списку ранее агрегированных интернет-заказов,
статистике и ранжированию интернет-магазинов.  
Браузерное расширение предоставляет возможность автоматического заполнения списка заказов.

## Project Description:

**Online Order Aggregator** is a system for keeping a personal history of orders from various online stores
, providing statistics, as well as the ability to rank the used online stores.

The system is a bundle of a website and a browser extension with a common server part.
The website provides access to a list of previously aggregated internet orders,
statistics, and ranking of online stores.  
The browser extension provides the ability to automatically fill in the order list.

![gif.gif](readme/gif.gif)

![img.png](readme/img.png)

![img_5.png](readme/img_5.png)

![img_1.png](readme/img_1.png)

![img_2.png](readme/img_2.png)

![img_4.png](readme/img_4.png)
