package sal.online_order_aggregator.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import sal.online_order_aggregator.entity.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class OrderUtils {
    public static int getDeliveryTimeRating(int deliveryTime) {
        if (deliveryTime <= 3) {
            return 5;
        } else if (deliveryTime <= 10) {
            return 4;
        } else if (deliveryTime <= 30) {
            return 3;
        } else if (deliveryTime <= 60) {
            return 2;
        } else {
            return 1;
        }
    }

    public static int getDeliveryTime(Order order) {
        Date oderDate = order.getOrderDate();
        Date receiveDate = order.getReceiveDate();

        long diff = TimeUnit.DAYS.convert(receiveDate.getTime() - oderDate.getTime(), TimeUnit.MILLISECONDS);

        return (int) diff;
    }

    public static List<MarketplaceRating> getMarketplaceRatings(Set<Marketplace> allMarkets,
                                                                List<OrderRating> avgRatings,
                                                                List<OrderRating> avgRatingsByUser,
                                                                int qualityFactor, int priceFactor,
                                                                int deliveryTimeFactor, int myRatingsFactor) {
        List<MarketplaceRating> list = new ArrayList<>();

        Map<String, OrderRating> avgRatingsMap = new HashMap<>();
        for (OrderRating o : avgRatings) avgRatingsMap.put(o.getMarketplaceName(), o);

        Map<String, OrderRating> avgRatingsByUserMap = new HashMap<>();
        for (OrderRating o : avgRatingsByUser) avgRatingsByUserMap.put(o.getMarketplaceName(), o);

        for (Marketplace market : allMarkets) {
            if (avgRatingsMap.get(market.getName()) == null) {
                list.add(getEmptyMarketplaceRating(market.getName()));
            } else {
                list.add(getMarketplaceRating(avgRatingsMap.get(market.getName()), avgRatingsByUserMap.get(market.getName()), qualityFactor, priceFactor, deliveryTimeFactor, myRatingsFactor));
            }
        }

        list.sort(Comparator.comparing(MarketplaceRating::getRating).reversed());

        return list;
    }

    private static MarketplaceRating getMarketplaceRating(OrderRating avgRating, OrderRating avgRatingByUser,
                                                          int qualityFactor, int priceFactor, int deliveryTimeFactor,
                                                          int myRatingsFactor) {
        MarketplaceRating rating = new MarketplaceRating();
        float overallRating;

        float userK = myRatingsFactor == 0 ? 0 : (float) myRatingsFactor / 10;
        float publicK = 1 - userK;
        if (avgRatingByUser == null) {
            publicK = 1;
            userK = 0;
        }

        float publicQualityRating = avgRating.getQualityRating();
        float publicPriceRating = avgRating.getPriceRating();
        float publicDeliveryTimeRating = avgRating.getDeliveryTimeRating();

        int sumOfFactors = qualityFactor + priceFactor + deliveryTimeFactor;
        if (sumOfFactors == 0) {
            sumOfFactors = 3;
            qualityFactor = 1;
            priceFactor = 1;
            deliveryTimeFactor = 1;
        }

        int factorMultiplier = 20;
        float realQualityFactor = (float) qualityFactor / sumOfFactors;
        float realPriceFactor = (float) priceFactor / sumOfFactors;
        float realDeliveryTimeFactor = (float) deliveryTimeFactor / sumOfFactors;

        if (publicK == 1) {
            overallRating = realQualityFactor * publicQualityRating
                    + realPriceFactor * publicPriceRating
                    + realDeliveryTimeFactor * publicDeliveryTimeRating;

            rating.setQualityRating(publicQualityRating);
            rating.setPriceRating(publicPriceRating);
            rating.setDeliveryTimeRating(publicDeliveryTimeRating);
        } else {
            float userQualityRating = avgRatingByUser.getQualityRating();
            float userPriceRating = avgRatingByUser.getPriceRating();
            float userDeliveryTimeRating = avgRatingByUser.getDeliveryTimeRating();

            if (publicK == 0) {
                overallRating = realQualityFactor * userQualityRating
                        + realPriceFactor * userPriceRating
                        + realDeliveryTimeFactor * userDeliveryTimeRating;

                rating.setQualityRating(userQualityRating);
                rating.setPriceRating(userPriceRating);
                rating.setDeliveryTimeRating(userDeliveryTimeRating);
            } else {
                float realQualityRating = ((publicQualityRating * publicK) + (userQualityRating * userK));
                float realPriceRating = ((publicPriceRating * publicK) + (userPriceRating * userK));
                float realDeliveryTimeRating = ((publicDeliveryTimeRating * publicK) + (userDeliveryTimeRating * userK));

                overallRating = realQualityFactor * realQualityRating
                        + realPriceFactor * realPriceRating
                        + realDeliveryTimeFactor * realDeliveryTimeRating;

                rating.setQualityRating(realQualityRating);
                rating.setPriceRating(realPriceRating);
                rating.setDeliveryTimeRating(realDeliveryTimeRating);
            }
        }

        rating.setName(avgRating.getMarketplaceName());
        rating.setRating(overallRating * factorMultiplier);

        return rating;
    }

    private static MarketplaceRating getEmptyMarketplaceRating(String market) {
        return new MarketplaceRating(market, 50, 2.5f, 2.5f, 2.5f);
    }

    public static Gson gsonParser = new GsonBuilder().setDateFormat("dd.MM.yyyy").create();
}
