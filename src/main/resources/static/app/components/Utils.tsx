import * as React from 'react';

import {Label, SemanticCOLORS} from 'semantic-ui-react';
import {DATE_FORMAT, EMarketplaces} from './interfaces';
import moment = require('moment');

export function getCookie(name) {
    let matches = document.cookie.match(
        new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)")
    );

    return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function getFullCSRFParam() {
    const token = getCookie('XSRF-TOKEN');

    return `_csrf=${token}`;
}

export function getCSRFToken() {
    return getCookie('XSRF-TOKEN');
}

export function getUsername() {
    return getCookie('username');
}

export function saveCookie(cookieName, value) {
    document.cookie = `${cookieName}=${value}; path=/`;
}

export function deleteCookie(cookieName) {
    document.cookie = `${cookieName}=${""}; path=/; expires=-1; Max-Age=-99999999;`;
}

export function deleteAllCookie() {
    deleteCookie("username");
    deleteCookie("XSRF-TOKEN");
}

export function isUserSignedIn() {
    let username = getCookie("username");
    return (username !== undefined);
}
export const getValueOrDash = (value: string) => {
    return value || '-';
};

export const getLabeledText = (label: string, text: string | JSX.Element) => {
    return (
        <>
            <div className={'labeled-text label'}>{label}</div>
            <div className={'labeled-text text'}>{text}</div>
        </>
    );
};

export const getMarketplaceLabel = (market: string) => {
    let color: SemanticCOLORS;

    switch (market) {
        case EMarketplaces.DNS:
            color = 'orange';
            break;
        case EMarketplaces.AliExpress:
            color = 'red';
            break;
        default:
            color = 'blue';
    }

    return (
        <Label basic color={color} style={{width: '100%'}} className={'centered'}>
            {market || 'ХЗ'}
        </Label>
    );
};

export const getDeliveryTime = (order: string, receive: string, received: boolean) => {
    if (received && order && receive) {
        const orderDate = moment(order, DATE_FORMAT);
        const receiveDate = moment(receive, DATE_FORMAT);
        return (receiveDate.diff(orderDate, 'days')).toString();
    }

    if (!received && order) {
        const orderDate = moment(order, DATE_FORMAT);
        const receiveDate = moment(moment(), DATE_FORMAT);
        return (receiveDate.diff(orderDate, 'days')).toString().concat(' +');
    }
};
