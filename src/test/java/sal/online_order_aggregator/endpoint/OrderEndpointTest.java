package sal.online_order_aggregator.endpoint;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

public class OrderEndpointTest {

    @Test
    public void addOrder() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(17);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void changeOrder() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(19);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void deleteOrder() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(10);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getOrdersClean() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(24);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getOrdersWithMarketplaceFilter() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(31);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getOrdersWithNameFilter() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(27);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getOrdersWithSort() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(33);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getOrdersWithMarketplaceFilterAndSort() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(29);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getOrdersWithNameFilterAndSort() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(24);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void getOrdersWithFiltersAndSort() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(35);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void addWarranty() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(13);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void setDelivered() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(12);
        Assertions.assertEquals(1, 1);
    }
}