package sal.online_order_aggregator.entity;

public class MarketplaceRating {
    private String name;
    private float rating;
    private float qualityRating;
    private float priceRating;
    private float deliveryTimeRating;

    public MarketplaceRating() {};

    public MarketplaceRating(String name, float rating, float qualityRating, float priceRating, float deliveryTimeRating) {
        this.name = name;
        this.rating = rating;
        this.qualityRating = qualityRating;
        this.priceRating = priceRating;
        this.deliveryTimeRating = deliveryTimeRating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public float getQualityRating() {
        return qualityRating;
    }

    public void setQualityRating(float qualityRating) {
        this.qualityRating = qualityRating;
    }

    public float getPriceRating() {
        return priceRating;
    }

    public void setPriceRating(float priceRating) {
        this.priceRating = priceRating;
    }

    public float getDeliveryTimeRating() {
        return deliveryTimeRating;
    }

    public void setDeliveryTimeRating(float deliveryTimeRating) {
        this.deliveryTimeRating = deliveryTimeRating;
    }
}
