package sal.online_order_aggregator.endpoint;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

public class MarketplaceRatingEndpointTest {

    @Test
    public void getRatings() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(21);
        Assertions.assertEquals(1, 1);
    }

    @Test
    public void addRatings() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(13);
        Assertions.assertEquals(1, 1);
    }
}