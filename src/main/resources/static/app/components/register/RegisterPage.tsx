import * as React from 'react';
import {Button, Input, Segment} from 'semantic-ui-react';
const axios = require('axios');

enum ErrorEnum {
    NONE = "",
    UNKNOWN = "Unknown error. Try again.",
    USERNAME_TAKEN = "This login is already taken.",
    LONG_USERNAME = "Login is too long (max length: 16).",
    LONG_NAME = "Name is too long (max length: 16).",
    WRONG_NAME = "Wrong name (allowed characters: a-Z, а-Я, 0-9, ' ').",
    WRONG_USERNAME = "Wrong login (allowed characters: a-Z, 0-9, _).",
    WRONG_EMAIL = "Wrong email (exemple: abc-42@xyz.com).",
    WRONG_PASS = "Wrong password (allowed characters: a-Z, а-Я, 0-9).",
    WRONG_PASS_LENGTH = "Password length must be 4-20 characters.",
    WRONG_PASS_REPEAT = "Passwords don't match."
}

interface IState {
    password: String;
    username: String;
    name: String;
    email: String;
    success: boolean;
    error: ErrorEnum;
}

interface IProps {
    history: any;
}

const w100 = {width: '100%'};

export class RegisterPage extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            password: '',
            username: '',
            name: '',
            email: '',
            success: false,
            error: ErrorEnum.NONE
        };
    }

    render() {
        return (
            <div className={'signin-wrapper'}>
                <div className={'signin'}>
                    <Segment className={'spaced'} style={{width: 300, padding: '40px 30px'}}>
                        <Input placeholder={'Логин'} style={w100} onChange={this.onChangeLogin}/>

                        <Input placeholder={'Пароль'} style={w100} onChange={this.onChangePass}/>

                        <Input placeholder={'Email'} style={w100} onChange={this.onChangeEmail}/>

                        <Button color={'green'} content={'Регистрация'} style={w100} onClick={this.onRegister}/>

                        <Button color={'blue'} content={'Уже зарегистрирован'} style={w100}
                                onClick={() => this.props.history.push('/signin')}/>
                    </Segment>
                </div>
            </div>
        );
    }

    onChangeLogin = (e, input) => {
        this.setState({username: input.value});
    };

    onChangePass = (e, input) => {
        this.setState({password: input.value});
    };

    onChangeEmail = (e, input) => {
        this.setState({email: input.value});
    };

    onRegister = () => {
        const {username, password, email} = this.state;

        axios.post('/api/addUser', {
            password, username, email
        })
            .then((response) => {
                let error = response.headers.error;

                if (error === 'none') {
                    this.props.history.push(`/signin`);
                }
                if (error === 'username_exists') {
                    alert('Такой логин уже занят');
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };
}
