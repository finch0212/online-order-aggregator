import * as React from 'react';
import {Button, Icon, Input, Modal, Select} from 'semantic-ui-react';
import {DATE_FORMAT, ECurrencies, EMarketplaces, EUserOrderParamNames, IOrder} from '../interfaces';
import {getUsername} from '../Utils';
import moment = require('moment');
const axios = require('axios');

interface IProps {
    reloadPage: () => void;
    onAddNewReceivedOrder: (order: IOrder) => void;
}

interface IState {
    id: string;
    modalOpen: boolean;
    link: string;
    name: string;
    orderDate: string;
    receiveDate: string,
    orderId: string;
    itemId: string;
    itemCount: number;
    picture: string;
    price: string;
    currency: string;
    warranty: string;
    marketplaceName: string;
    track: string;
    received: boolean;
    isEditing: boolean;
    rated: boolean;
}

const defaultState = {
    modalOpen: false,
    isEditing: false,
    id: '',
    marketplaceName: undefined,
    name: undefined,
    orderDate: undefined,
    receiveDate: undefined,
    itemCount: undefined,
    price: undefined,
    currency: undefined,
    link: undefined,
    orderId: undefined,
    itemId: undefined,
    picture: undefined,
    warranty: undefined,
    track: undefined,
    received: undefined,
    rated: false,
};

export class AddNewOrderModal extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = defaultState;
    }

    open = () => {
        this.setState({...defaultState, modalOpen: true});
    };

    openForEdit = (order: IOrder) => {
        let orderDate = moment(order.orderDate, DATE_FORMAT).format('YYYY-MM-DD');
        let receiveDate = order.receiveDate ? moment(order.receiveDate, DATE_FORMAT).format('YYYY-MM-DD') : null;

        this.setState({
            modalOpen: true,
            isEditing: true,
            id: order.id || undefined,
            marketplaceName: EMarketplaces[order.marketplaceName],
            name: order.name || undefined,
            orderDate: orderDate || undefined,
            receiveDate: receiveDate || undefined,
            itemCount: order.itemCount || undefined,
            price: order.price || undefined,
            currency: order.currency || undefined,
            link: order.link || undefined,
            orderId: order.orderId || undefined,
            itemId: order.itemId || undefined,
            picture: order.picture || undefined,
            warranty: order.warranty || undefined,
            track: order.track || undefined,
            received: order.received || undefined,
            rated: order.rated,
        });
    };

    render() {
        const {} = this.props;
        const {modalOpen, isEditing} = this.state;

        return (
            <>
                <Button basic color={'blue'} onClick={this.open}><Icon name={'plus'}/> Добавить заказ</Button>

                <Modal
                    open={modalOpen}
                    size={'small'}
                    onClose={() => this.setState({modalOpen: false})}
                >
                    <Modal.Header>{isEditing ? 'Изменить' : 'Добавить новый'} заказ</Modal.Header>
                    <Modal.Content className={'spaced'}>
                        {this.getNameInput()}
                        {this.getMarketplaceSelect()}
                        {this.getOrderIdInput()}
                        <div style={{display: 'flex', justifyContent: 'space-between'}}>
                            {this.getItemCountInput()}
                            {this.getPriceInput()}
                            {this.getCurrencySelect()}
                            {this.getReceivedSelect()}
                        </div>
                        {this.getOrderDateInput()}
                        {this.state.received && this.getReceiveDateInput()}
                        {this.getPictureInput()}
                        {this.getLinkInput()}
                        {this.getTrackInput()}
                        {this.getWarrantyInput()}

                        <Button
                            content={isEditing ? 'Изменить заказ' : 'Добавить заказ'}
                            basic
                            color={'blue'}
                            onClick={this.handleSendOrder}
                        />
                    </Modal.Content>
                </Modal>
            </>
        );
    }

    getMarketplaceSelect = () => {
        const {marketplaceName, isEditing} = this.state;

        return (
            <Select
                disabled={isEditing}
                style={{width: '49%'}}
                options={
                    Object.keys(EMarketplaces).map((marketName, index) => {
                        return {key: index, value: marketName, text: marketName};
                    })
                }
                value={marketplaceName}
                onChange={(e, data) => {
                    this.setState({marketplaceName: data.value.toString()});
                }}
                placeholder={EUserOrderParamNames.marketplace}
                className={'required-star'}
            />
        );
    };

    getCurrencySelect = () => {
        const {currency, isEditing} = this.state;

        return (
            <Select
                disabled={isEditing}
                style={{width: '24%'}}
                options={
                    Object.keys(ECurrencies).map((currency, index) => {
                        return {key: index, value: currency, text: currency};
                    })
                }
                value={currency}
                onChange={(e, data) => {
                    this.setState({currency: data.value.toString()});
                }}
                placeholder={EUserOrderParamNames.currency}
                className={'required-star'}
            />
        );
    };

    getReceivedSelect = () => {
        const {received, isEditing} = this.state;

        return (
            <Select
                style={{width: '24%'}}
                options={[
                    {key: 1, value: true, text: 'Получен'},
                    {key: 2, value: false, text: 'В пути'},
                ]
                }
                value={received}
                onChange={(e, data) => {
                    this.setState({received: Boolean(data.value)});
                }}
                placeholder={EUserOrderParamNames.received}
                className={'required-star'}
            />
        );
    };

    getNameInput = () => {
        const {isEditing, name} = this.state;

        return (
            <Input
                style={{width: '100%'}}
                onChange={(e, ch) => {
                    this.setState({name: ch.value});
                }}
                value={name}
                placeholder={EUserOrderParamNames.name}
                className={'required-star'}
            />
        );
    };

    getOrderIdInput = () => {
        const {isEditing, orderId} = this.state;

        return (
            <Input
                style={{width: '49%', float: 'right'}}
                disabled={isEditing}
                onChange={(e, ch) => {
                    this.setState({orderId: ch.value});
                }}
                value={orderId}
                placeholder={EUserOrderParamNames.orderId}
                className={'required-star'}
            />
        );
    };

    getOrderDateInput = () => {
        const {isEditing, orderDate} = this.state;

        return (
            <Input
                style={{width: '100%'}}
                disabled={isEditing}
                type={'date'}
                onChange={(e, ch) => {
                    this.setState({orderDate: ch.value});
                }}
                value={orderDate}
                placeholder={EUserOrderParamNames.orderDate}
                className={'required-star'}
            />
        );
    };

    getReceiveDateInput = () => {
        const {isEditing, receiveDate} = this.state;

        return (
            <Input
                style={{width: '100%'}}
                type={'date'}
                onChange={(e, ch) => {
                    this.setState({receiveDate: ch.value});
                }}
                value={receiveDate}
                placeholder={EUserOrderParamNames.receiveDate}
                className={'required-star'}
            />
        );
    };

    getItemCountInput = () => {
        const {isEditing, itemCount} = this.state;

        return (
            <Input
                style={{width: '19%'}}
                type={'number'}
                min={'1'}
                max={'1000'}
                onChange={(e, ch) => {
                    this.setState({itemCount: ch.value as unknown as number});
                }}
                value={itemCount}
                placeholder={EUserOrderParamNames.itemCount}
                className={'required-star'}
            />
        );
    };

    getPriceInput = () => {
        const {isEditing, price} = this.state;

        return (
            <Input
                style={{width: '19%'}}
                type={'number'}
                min={'1'}
                onChange={(e, ch) => {
                    this.setState({price: ch.value});
                }}
                value={price}
                placeholder={EUserOrderParamNames.price}
                className={'required-star'}
            />
        );
    };

    getTrackInput = () => {
        return (
            <Input
                style={{width: '49%'}}
                onChange={(e, ch) => {
                    this.setState({track: ch.value});
                }}
                value={this.state.track}
                placeholder={EUserOrderParamNames.track}
            />
        );
    };

    getPictureInput = () => {
        return (
            <Input
                style={{width: '100%'}}
                onChange={(e, ch) => {
                    this.setState({picture: ch.value});
                }}
                value={this.state.picture}
                placeholder={EUserOrderParamNames.picture}
            />
        );
    };

    getLinkInput = () => {
        return (
            <Input
                style={{width: '100%'}}
                onChange={(e, ch) => {
                    this.setState({link: ch.value});
                }}
                value={this.state.link}
                placeholder={EUserOrderParamNames.link}
            />
        );
    };

    getWarrantyInput = () => {
        return (
            <Input
                style={{width: '49%', float: 'right'}}
                onChange={(e, ch) => {
                    this.setState({warranty: ch.value});
                }}
                value={this.state.warranty}
                placeholder={EUserOrderParamNames.warranty}
            />
        );
    };

    handleSendOrder = () => {
        if (!this.isOrderValid()) {
            alert('Заполните необходимые поля');

            return;
        }

        const username = getUsername();

        const orders: IOrder[] = [{
            id: this.state.isEditing ? this.state.id : null,
            link: this.state.link,
            name: this.state.name,
            orderDate: this.state.orderDate,
            receiveDate: this.state.receiveDate,
            orderId: this.state.orderId,
            itemId: this.state.itemId,
            itemCount: this.state.itemCount,
            picture: this.state.picture,
            price: this.state.price,
            currency: this.state.currency,
            warranty: this.state.warranty,
            track: this.state.track,
            received: this.state.received,
            marketplaceName: EMarketplaces[this.state.marketplaceName],
        }];

        axios.post(this.state.isEditing ? 'api/changeOrder?' : `api/addOrders?`, orders, {
            params: {username},
            timeout: 5000,
        })
            .then((response) => {
                this.setState({modalOpen: false});
                if (this.state.received && !this.state.rated && this.state.marketplaceName !== EMarketplaces.Other) {
                    let order: IOrder = response.data.value;
                    if (orders[0].id) {
                        order = orders[0];
                    }

                    this.props.onAddNewReceivedOrder(order);
                } else {
                    this.props.reloadPage();
                }
            })
            .catch((error) => {
                alert('FAIL');
            });
    };

    isOrderValid = () => {
        if (!this.state.name || !this.state.marketplaceName || !this.state.orderId || !this.state.itemCount
            || !this.state.price || !this.state.currency || this.state.received === null || !this.state.orderDate) {
            return false;
        }

        return !(this.state.received && !this.state.receiveDate);
    };
}
