import * as React from 'react';
import {Button, Input, Modal} from 'semantic-ui-react';
const axios = require('axios');

interface IProps {
    reloadPage: () => void;
}

interface IState {
    track: string;
    modalOpen: boolean;
    editedOrderId: string;
}

export class AddTrackModal extends React.PureComponent<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            track: '',
            modalOpen: false,
            editedOrderId: null,
        };
    }

    open = (editedOrderId: string, track: string) => {
        this.setState({modalOpen: true, editedOrderId, track});
    };

    render() {
        const {} = this.props;
        const {modalOpen, track} = this.state;

        return (
            <Modal
                open={modalOpen}
                size='tiny'
                onClose={() => this.setState({modalOpen: false})}
            >
                <Modal.Header>Изменить трек-код</Modal.Header>
                <Modal.Content className={'spaced'}>
                    <Input style={{width: '100%'}}
                           onChange={(e, d) => {
                               this.setState({track: d.value});
                           }}
                           value={track}
                    />
                    <Button color={'blue'} content={'Изменить'} basic onClick={this.handleTrackSend}/>
                </Modal.Content>
            </Modal>
        );
    }

    handleTrackSend = () => {
        const {track, editedOrderId: id} = this.state;

        axios.post(`api/addTrack`, {track, id}, {
            timeout: 5000,
        })
            .then((response) => {
                this.setState({modalOpen: false});
                this.props.reloadPage();
            })
            .catch((error) => {
            });
    };
}
